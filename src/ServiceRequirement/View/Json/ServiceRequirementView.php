<?php
namespace ProductMarket\ServiceRequirement\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\ServiceRequirement\View\ServiceRequirementViewTrait;

class ServiceRequirementView extends JsonView implements IView
{
    use ServiceRequirementViewTrait;
    
    public function display() : void
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getServiceRequirement()
        );

        $this->encode($data);
    }
}
