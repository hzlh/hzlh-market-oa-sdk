<?php
namespace ProductMarket\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Framework\Classes\NullCommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;

class ServiceRequirementCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'ProductMarket\ServiceRequirement\Command\ServiceRequirement\RejectServiceRequirementCommand'=>
        'ProductMarket\ServiceRequirement\CommandHandler\ServiceRequirement\RejectServiceRequirementCommandHandler',
        'ProductMarket\ServiceRequirement\Command\ServiceRequirement\ApproveServiceRequirementCommand'=>
        'ProductMarket\ServiceRequirement\CommandHandler\ServiceRequirement\ApproveServiceRequirementCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : new NullCommandHandler();
    }
}
