<?php
namespace ProductMarket\ServiceRequirement\Controller;

use Common\Controller\Interfaces\IFetchAbleController;
use Common\Controller\Traits\FetchControllerTrait;
use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Interfaces\INull;

use ProductMarket\ServiceRequirement\View\Json\ServiceRequirementListView;
use ProductMarket\ServiceRequirement\View\Json\ServiceRequirementView;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\ProductMarket\ServiceRequirement\Repository\ServiceRequirementRepository;

use Sdk\Role\Model\IRoleAble;
use Common\Controller\Traits\GlobalRoleCheckTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalRoleCheckTrait;

    const SCENE = array(
        'ALL' => 0,
        'PENDING' => 1,
        'APPROVE' => 2,
        'REJECT' => 3,
    );

    const SEARCH = array(
        'TITLE' => 'title',
        'NUMBER' => 'number'
    );

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRequirementRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository(): ServiceRequirementRepository
    {
        return $this->repository;
    }

    protected function filterAction()
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['SERVICE_REQUIREMENT'],
        //     IRoleAble::OPERATION['OPERATION_SHOW']
        // )
        // ) {
        //     return false;
        // }

        $search = $this->getRequest()->get('search', array());
        $searchType = $this->getRequest()->get('searchType', '');
        $scene = $this->getRequest()->get('scene', '');
        $scene = marmot_decode($scene);

        list($size, $page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($searchType, $search, $scene);

        $serviceRequirementList = array();

        list($count, $serviceRequirementList) = $this->getRepository()
            ->scenario(ServiceRequirementRepository::OA_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
            
        $this->render(new ServiceRequirementListView($serviceRequirementList, $count));
        return true;
    }

    protected function filterFormatChange($searchType, $search, $scene)
    {
        $sort = ['-updateTime'];
        $filter = array();

        if ($scene != self::SCENE['ALL']) {
            $filter['status'] = IModifyStatusAble::STATUS['NORMAL'];
        }

        if ($scene == self::SCENE['PENDING']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'];
        }
        if ($scene == self::SCENE['APPROVE']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        }
        if ($scene == self::SCENE['REJECT']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['REJECT'];
        }

        if (!empty($search[0])) {
            if ($searchType == self::SEARCH['TITLE']) {
                $filter['title'] = $search[0];
            }
            if ($searchType == self::SEARCH['NUMBER']) {
                $filter['number'] = $search[0];
            }
        }

        return [$filter, $sort];
    }

    protected function fetchOneAction(int $id)
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['SERVICE_REQUIREMENT'],
        //     IRoleAble::OPERATION['OPERATION_SHOW']
        // )
        // ) {
        //     return false;
        // }

        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $serviceRequirement = $this->getRepository()
        ->scenario(ServiceRequirementRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        if ($serviceRequirement instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new ServiceRequirementView($serviceRequirement));
        return true;
    }
}
