<?php
namespace ProductMarket\ServiceCategory\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\ServiceCategory\Model\ServiceCategory;
use Sdk\ProductMarket\ServiceCategory\Model\NullServiceCategory;

use ProductMarket\ParentCategory\Translator\ParentCategoryTranslator;

class ServiceCategoryTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $serviceCategory = null)
    {
        unset($serviceCategory);
        unset($expression);
        return NullServiceCategory::getInstance();
    }

    protected function getParentCategoryTranslator() : ParentCategoryTranslator
    {
        return new ParentCategoryTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($serviceCategory, array $keys = array())
    {
        if (!$serviceCategory instanceof ServiceCategory) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'isQualification',
                'isEnterpriseVerify',
                'qualificationName',
                'commission',
                'parentCategory'=>['id', 'name'],
                'createTime',
                'updateTime',
                'statusTime',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] =  marmot_encode($serviceCategory->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $serviceCategory->getName();
        }
        if (in_array('isQualification', $keys)) {
            $expression['isQualification'] = $serviceCategory->getIsQualification();
        }
        if (in_array('isEnterpriseVerify', $keys)) {
            $expression['isEnterpriseVerify'] = $serviceCategory->getIsEnterpriseVerify();
        }
        if (in_array('qualificationName', $keys)) {
            $expression['qualificationName'] = $serviceCategory->getQualificationName();
        }
        if (in_array('commission', $keys)) {
            $expression['commission'] = $serviceCategory->getCommission();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $serviceCategory->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $serviceCategory->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $serviceCategory->getStatusTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $serviceCategory->getStatus();
        }
        if (isset($keys['parentCategory'])) {
            $expression['parentCategory'] = $this->getParentCategoryTranslator()->objectToArray(
                $serviceCategory->getParentCategory(),
                $keys['parentCategory']
            );
        }

        return $expression;
    }
}
