<?php
namespace ProductMarket\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\ProductMarket\ServiceCategory\Model\ParentCategory;
use Sdk\ProductMarket\ServiceCategory\Repository\ParentCategoryRepository;
use Sdk\ProductMarket\ServiceCategory\Model\ServiceCategory;
use ProductMarket\ServiceCategory\Command\ServiceCategory\AddServiceCategoryCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class AddServiceCategoryCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;

    private $serviceCategory;

    private $parentCategoryRepository;

    public function __construct()
    {
        $this->serviceCategory = new ServiceCategory();
        $this->parentCategoryRepository = new ParentCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->serviceCategory);
        unset($this->parentCategoryRepository);
    }

    protected function getServiceCategory() : ServiceCategory
    {
        return $this->serviceCategory;
    }

    protected function getParentCategoryRepository() : ParentCategoryRepository
    {
        return $this->parentCategoryRepository;
    }

    private function fetchParentCategory(int $id) : ParentCategory
    {
        return $this->getParentCategoryRepository()->fetchOne($id);
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddServiceCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $parentCategory = $this->fetchParentCategory($command->parentCategoryId);
        $serviceCategory = $this->getServiceCategory();

        $serviceCategory->setParentCategory($parentCategory);
        $serviceCategory->setName($command->name);
        $serviceCategory->setQualificationName($command->qualificationName);
        $serviceCategory->setIsQualification($command->isQualification);
        $serviceCategory->setIsEnterpriseVerify($command->isEnterpriseVerify);
        $serviceCategory->setCommission($command->commission);
        $serviceCategory->setStatus($command->status);

        if ($serviceCategory->add()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_ADD'],
            ILogAble::CATEGORY['SERVICE_CATEGORY'],
            $this->getServiceCategory()->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->getServiceCategory()->getName()
        );
    }
}
