<?php
namespace ProductMarket\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use ProductMarket\ServiceCategory\Command\ServiceCategory\EditServiceCategoryCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class EditServiceCategoryCommandHandler implements ICommandHandler, ILogAble
{
    use ServiceCategoryCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditServiceCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->serviceCategory = $this->fetchServiceCategory($command->id);
        $this->serviceCategory->setName($command->name);
        $this->serviceCategory->setQualificationName($command->qualificationName);
        $this->serviceCategory->setIsEnterpriseVerify($command->isEnterpriseVerify);
        $this->serviceCategory->setIsQualification($command->isQualification);
        $this->serviceCategory->setCommission($command->commission);
        $this->serviceCategory->setStatus($command->status);

        if ($this->serviceCategory->edit()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_EDIT'],
            ILogAble::CATEGORY['SERVICE_CATEGORY'],
            $this->serviceCategory->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->serviceCategory->getName()
        );
    }
}
