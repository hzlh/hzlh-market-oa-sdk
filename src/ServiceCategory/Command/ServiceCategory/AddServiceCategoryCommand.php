<?php
namespace ProductMarket\ServiceCategory\Command\ServiceCategory;

use Marmot\Interfaces\ICommand;

class AddServiceCategoryCommand implements ICommand
{
    public $parentCategoryId;

    public $name;

    public $isEnterpriseVerify;

    public $isQualification;

    public $qualificationName;

    public $commission;

    public $status;

    public $id;

    public function __construct(
        int $parentCategoryId,
        string $name,
        string $qualificationName,
        int $isEnterpriseVerify,
        int $isQualification = 0,
        float $commission = 0,
        int $status = 0,
        int $id = 0
    ) {
        $this->parentCategoryId = $parentCategoryId;
        $this->name = $name;
        $this->qualificationName = $qualificationName;
        $this->isEnterpriseVerify = $isEnterpriseVerify;
        $this->isQualification = $isQualification;
        $this->commission = $commission;
        $this->status = $status;
        $this->id = $id;
    }
}
