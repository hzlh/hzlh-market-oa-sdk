<?php
namespace ProductMarket\ServiceCategory\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use ProductMarket\ServiceCategory\View\Json\ServiceCategoryView;
use ProductMarket\ServiceCategory\View\Json\ServiceCategoryListView;
use Sdk\ProductMarket\ServiceCategory\Repository\ServiceCategoryRepository;
use Sdk\Common\Model\IEnableAble;

use Sdk\Role\Model\IRoleAble;
use Common\Controller\Traits\GlobalRoleCheckTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalRoleCheckTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }

    protected function filterAction()
    {
        $search = $this->getRequest()->get('search', array());

        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($search);

        $dispatchDepartmentList = array();

        list($count, $dispatchDepartmentList) =
            $this->getRepository()
            ->scenario(ServiceCategoryRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ServiceCategoryListView($dispatchDepartmentList, $count));
        return true;
    }

    protected function filterFormatChange($search)
    {
        $parentCategoryId = $this->getRequest()->get('parentCategoryId', '');

        $sort = ['-updateTime'];
        $filter = array();

        // $filter['status'] = IEnableAble::STATUS['ENABLED'];
        if (!empty($search[0])) {
            $filter['name'] = $search[0];
        }

        if (!empty($parentCategoryId)) {
            $filter['parentCategory'] = marmot_decode($parentCategoryId);
        }

        return [$filter, $sort];
    }

    protected function fetchOneAction(int $id)
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $dispatchDepartment = $this->getRepository()
                              ->scenario(ServiceCategoryRepository::FETCH_ONE_MODEL_UN)
                              ->fetchOne($id);

        if ($dispatchDepartment instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new ServiceCategoryView($dispatchDepartment));
        return true;
    }
}
