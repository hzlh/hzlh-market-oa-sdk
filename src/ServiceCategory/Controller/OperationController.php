<?php
namespace ProductMarket\ServiceCategory\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use ProductMarket\ServiceCategory\View\Json\AddView;
use ProductMarket\ServiceCategory\View\Json\EditView;
use ProductMarket\ServiceCategory\Command\ServiceCategory\AddServiceCategoryCommand;
use ProductMarket\ServiceCategory\Command\ServiceCategory\EditServiceCategoryCommand;
use ProductMarket\ServiceCategory\CommandHandler\ServiceCategory\ServiceCategoryCommandHandlerFactory;

use Sdk\ProductMarket\ServiceCategory\Repository\ParentCategoryRepository;
use Sdk\ProductMarket\ServiceCategory\Model\ServiceCategory;
use Sdk\ProductMarket\ServiceCategory\Repository\ServiceCategoryRepository;

use Common\Controller\Interfaces\IOperatAbleController;
use Common\Controller\Traits\OperatControllerTrait;
use Common\Controller\Traits\GlobalRoleCheckTrait;

use WidgetRules\ServiceCategory\WidgetRules as ServiceCategoryWidgetRules;

class OperationController extends Controller implements IOperatAbleController
{
    use WebTrait, OperatControllerTrait, OperatorControllerTrait, GlobalRoleCheckTrait;

    private $commandBus;

    private $serviceCategoryWidgetRules;

    private $repository;

    private $parentCategoryRepository;
    
    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceCategoryCommandHandlerFactory());
        $this->serviceCategoryWidgetRules = new ServiceCategoryWidgetRules();
        $this->repository = new ServiceCategoryRepository();
        $this->parentCategoryRepository = new ParentCategoryRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getServiceCategoryWidgetRules() : ServiceCategoryWidgetRules
    {
        return $this->serviceCategoryWidgetRules;
    }

    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }

    protected function getParentCategoryRepository() : ParentCategoryRepository
    {
        return $this->parentCategoryRepository;
    }

    protected function addView() : bool
    {
        $parentCategories = $this->fetchParentCategories();

        $this->render(new AddView($parentCategories));
        return true;
    }

    protected function addAction() : bool
    {
        $request = $this->getRequest();

        $parentCategoryId = marmot_decode($request->post('parentCategoryId', ''));

        $name = $request->post('name', '');
        $qualificationName = $request->post('qualificationName', '');
        $isEnterpriseVerify = $request->post('isEnterpriseVerify', ServiceCategory::IS_ENTERPRISE_VERIFY['YES']);
        $isQualification = $request->post('isQualification', 0);
        $commission = $request->post('commission', 0.00);
        $status = $request->post('status', 0);
       
        if ($this->validateCommonScenario(
            $name,
            $qualificationName,
            $isEnterpriseVerify,
            $isQualification,
            $commission
        )) {
            $command = new AddServiceCategoryCommand(
                $parentCategoryId,
                $name,
                $qualificationName,
                $isEnterpriseVerify,
                $isQualification,
                $commission,
                $status
            );
           
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        $this->displayError();
        return false;
    }

    protected function validateCommonScenario(
        $name,
        $qualificationName,
        $isEnterpriseVerify,
        $isQualification,
        $commission
    ) {
        return $this->getServiceCategoryWidgetRules()->name($name)
        && $this->getServiceCategoryWidgetRules()->qualificationName($isQualification, $qualificationName)
        && $this->getServiceCategoryWidgetRules()->isQualification($isQualification)
        && $this->getServiceCategoryWidgetRules()->isEnterpriseVerify($isEnterpriseVerify)
        && $this->getServiceCategoryWidgetRules()->commission($commission);
    }

    protected function editView(int $id) : bool
    {
        $serviceCategory = $this->getRepository()
                              ->scenario(ServiceCategoryRepository::FETCH_ONE_MODEL_UN)
                              ->fetchOne($id);

        if ($serviceCategory instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $parentCategories = $this->fetchParentCategories();

        $this->render(new EditView($serviceCategory, $parentCategories));
        return true;
    }

    protected function editAction(int $id) : bool
    {
        $request = $this->getRequest();

        $name = $request->post('name', '');
        $qualificationName = $request->post('qualificationName', '');
        $isQualification = $request->post('isQualification', 0);
        $isEnterpriseVerify = $request->post('isEnterpriseVerify', ServiceCategory::IS_ENTERPRISE_VERIFY['YES']);
        $commission = $request->post('commission', 0.00);
        $status = $request->post('status', 0);
        
        if ($this->validateCommonScenario(
            $name,
            $qualificationName,
            $isEnterpriseVerify,
            $isQualification,
            $commission
        )) {
            $command = new EditServiceCategoryCommand(
                $name,
                $qualificationName,
                $isEnterpriseVerify,
                $isQualification,
                $commission,
                $status,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        $this->displayError();
        return false;
    }
}
