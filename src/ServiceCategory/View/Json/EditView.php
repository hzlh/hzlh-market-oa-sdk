<?php
namespace ProductMarket\ServiceCategory\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\ProductMarket\ServiceCategory\Model\ServiceCategory;
use ProductMarket\ServiceCategory\Translator\ServiceCategoryTranslator;
use ProductMarket\ParentCategory\Translator\ParentCategoryTranslator;

class EditView extends JsonView implements IView
{
    private $serviceCategory;

    private $serviceCategoryTranslator;

    private $parentCategories;

    private $parentCategoryTranslator;

    public function __construct(ServiceCategory $serviceCategory, array $parentCategories)
    {
        $this->serviceCategory = $serviceCategory;
        $this->parentCategories = $parentCategories;
        $this->serviceCategoryTranslator = new ServiceCategoryTranslator();
        $this->parentCategoryTranslator = new ParentCategoryTranslator();
        parent::__construct();
    }

    public function __destruct()
    {
        unset($this->serviceCategory);
        unset($this->parentCategories);
        unset($this->serviceCategoryTranslator);
        unset($this->parentCategoryTranslator);
    }

    protected function getServiceCategory() : ServiceCategory
    {
        return $this->serviceCategory;
    }

    protected function getParentCategories() : array
    {
        return $this->parentCategories;
    }

    protected function getServiceCategoryTranslator() : ServiceCategoryTranslator
    {
        return $this->serviceCategoryTranslator;
    }

    protected function getParentCategoryTranslator() : ParentCategoryTranslator
    {
        return $this->parentCategoryTranslator;
    }

    public function display() : void
    {
        $parentCategories = $this->getParentCategories();
        
        foreach ($parentCategories as $parentCategory) {
            $parentCategoryArray[] = $this->getParentCategoryTranslator()->ObjectToArray(
                $parentCategory,
                array('id', 'name')
            );
        }

        $serviceCategoryArray = $this->getServiceCategoryTranslator()->ObjectToArray($this->getServiceCategory());

        $data['parentCategories'] = $parentCategoryArray;
        $data['serviceCategory'] = $serviceCategoryArray;

        $this->encode($data);
    }
}
