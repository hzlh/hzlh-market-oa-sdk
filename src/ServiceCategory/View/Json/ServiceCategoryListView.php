<?php
namespace ProductMarket\ServiceCategory\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\ServiceCategory\Translator\ServiceCategoryTranslator;

class ServiceCategoryListView extends JsonView implements IView
{
    private $serviceCategoryList;

    private $count;

    private $translator;

    public function __construct(
        array $serviceCategoryList,
        int $count
    ) {
        $this->serviceCategoryList = $serviceCategoryList;
        $this->count = $count;
        $this->translator = new ServiceCategoryTranslator();
        parent::__construct();
    }

    protected function getServiceCategoryList() : array
    {
        return $this->serviceCategoryList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : ServiceCategoryTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getTranslator();

        foreach ($this->getServiceCategoryList() as $serviceCategory) {
            $data[] = $translator->objectToArray(
                $serviceCategory,
                array(
                    'id',
                    'name',
                    'isQualification',
                    'isEnterpriseVerify',
                    'parentCategory'=>['id', 'name'],
                    'updateTime',
                    'status'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
