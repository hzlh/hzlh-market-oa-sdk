<?php
namespace ProductMarket\TradeRecord\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use ProductMarket\TradeRecord\View\Json\TradeRecordView;
use ProductMarket\TradeRecord\View\Json\TradeRecordListView;

use Sdk\ProductMarket\TradeRecord\Repository\TradeRecordRepository;

use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\Enterprise\Model\Enterprise;

use Common\Controller\Traits\GlobalRoleCheckTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalRoleCheckTrait;

    private $repository;

    private $enterpriseRepository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new TradeRecordRepository();
        $this->enterpriseRepository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->enterpriseRepository);
    }

    protected function getRepository() : TradeRecordRepository
    {
        return $this->repository;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function filterAction()
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['TRADE_RECORD'],
        //     IRoleAble::OPERATION['OPERATION_SHOW']
        // )
        // ) {
        //     return false;
        // }

        list($size, $page) = $this->getPageAndSize();

        list($filter, $sort) = $this->filterFormatChange();

        list($count, $tradeRecordList) = $this->getRepository()
            ->scenario(TradeRecordRepository::OA_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
             
        $this->render(new TradeRecordListView($tradeRecordList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $type = $this->getRequest()->get('type', '');
        $startTime = $this->getRequest()->get('startTime', '');
        $endTime = $this->getRequest()->get('endTime', '');

        $sort = ['-updateTime'];
        $filter = array();

        if (!empty($type)) {
            $filter['type'] = $type;
        }

        if (!empty($startTime)) {
            $filter['startTime'] = $startTime;
        }

        if (!empty($endTime)) {
            $filter['endTime'] = $endTime;
        }

        return [$filter, $sort];
    }

    protected function fetchOneAction($id)
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['TRADE_RECORD'],
        //     IRoleAble::OPERATION['OPERATION_SHOW']
        // )
        // ) {
        //     return false;
        // }
       
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $tradeRecord = $this->getRepository()
                    ->scenario(TradeRecordRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
       
        if ($tradeRecord instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $type = $tradeRecord->getType();
        //因账户id即为企业id,所以这块只需要获取账户id就可以获取企业信息
        $enterpriseId = $tradeRecord->getMemberAccount()->getId();
        $enterprise = $this->fetchEnterprise($enterpriseId);

        $this->render(new TradeRecordView($type, $tradeRecord, $enterprise));
        return true;
    }

    private function fetchEnterprise($enterpriseId): Enterprise
    {
        $enterprise = $this->getEnterpriseRepository()
                    ->scenario(EnterpriseRepository::FETCH_ONE_MODEL_UN)->fetchOne($enterpriseId);

        return $enterprise;
    }
}
