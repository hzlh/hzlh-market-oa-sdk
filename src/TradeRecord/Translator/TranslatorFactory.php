<?php
namespace ProductMarket\TradeRecord\Translator;

use Sdk\ProductMarket\TradeRecord\Model\TradeRecord;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\NullTranslator;

class TranslatorFactory
{
    const MAPS = array(
        TradeRecord::TRADE_RECORD_TYPES['DEPOSIT'] =>
            'Deposit\Translator\DepositTranslator',
        TradeRecord::TRADE_RECORD_TYPES['DEPOSIT_PLATFORM'] =>
            'Deposit\Translator\DepositTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_ENTERPRISE'] =>
            'ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_PAY_PLATFORM'] =>
            'ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_BUYER_ENTERPRISE'] =>
            'ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SELLER_ENTERPRISE'] =>
            'ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_PLATFORM'] =>
            'ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER_CONFIRMATION_SUBSIDY'] =>
            'ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator',
        TradeRecord::TRADE_RECORD_TYPES['PLATFORM_REFUND'] =>
            'ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator',
        TradeRecord::TRADE_RECORD_TYPES['BUYER_REFUND'] =>
            'ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator',
        TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL'] =>
            'Withdrawal\Translator\WithdrawalTranslator',
        TradeRecord::TRADE_RECORD_TYPES['WITHDRAWAL_PLATFORM'] =>
            'Withdrawal\Translator\WithdrawalTranslator',
    );

    public function getTranslator(string $type) : ITranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : NullTranslator::getInstance();
    }
}
