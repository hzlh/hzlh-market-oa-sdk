<?php
namespace ProductMarket\TradeRecord\Translator;

use Marmot\Interfaces\ITranslator;

use ProductMarket\MemberAccount\Translator\MemberAccountTranslator;
use Sdk\ProductMarket\TradeRecord\Model\TradeRecord;
use Sdk\ProductMarket\TradeRecord\Model\NullTradeRecord;

class TradeRecordTranslator implements ITranslator
{
    protected function getMemberAccountTranslator() : MemberAccountTranslator
    {
        return new MemberAccountTranslator();
    }

    public function getTranslatorFactory() : TranslatorFactory
    {
        return new TranslatorFactory();
    }

    public function arrayToObject(array $expression, $tradeRecord = null)
    {
        unset($expression);
        unset($tradeRecord);
        return NullTradeRecord::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($tradeRecord, array $keys = array())
    {
        if (!$tradeRecord instanceof TradeRecord) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'tradeTime',
                'tradeType',
                'tradeMoney',
                'debtor',
                'creditor',
                'balance',
                'comment',
                'reference'=>[],
                'memberAccount'=>[]
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($tradeRecord->getId());
        }
        if (in_array('tradeTime', $keys)) {
            $expression['tradeTime'] = $tradeRecord->getTradeTime();
            $expression['tradeTimeFormat'] = date('Y-m-d H:i:s', $expression['tradeTime']);
        }
        if (in_array('tradeType', $keys)) {
            $expression['tradeType'] = $tradeRecord->getType();
        }
        if (in_array('tradeMoney', $keys)) {
            $expression['tradeMoney'] = $tradeRecord->getTradeMoney();
            if (strpos($expression['tradeMoney'], '.') === false) {
                $expression['tradeMoney'] = $expression['tradeMoney'].'.00';
            }
        }
        if (in_array('debtor', $keys)) {
            $expression['debtor'] = $tradeRecord->getDebtor();
        }
        if (in_array('creditor', $keys)) {
            $expression['creditor'] = $tradeRecord->getCreditor();
        }
        if (in_array('balance', $keys)) {
            $expression['balance'] = $tradeRecord->getBalance();
            if (strpos($expression['balance'], '.') === false) {
                $expression['balance'] = $expression['balance'].'.00';
            }
        }
        if (in_array('comment', $keys)) {
            $expression['comment'] = $tradeRecord->getComment();
        }

        if (isset($keys['memberAccount'])) {
            $expression['memberAccount'] = $this->getMemberAccountTranslator()->objectToArray(
                $tradeRecord->getMemberAccount(),
                $keys['memberAccount']
            );
        }

        if (isset($keys['reference'])) {
            $translator = $this->getTranslatorFactory()->getTranslator($tradeRecord->getType());

            $expression['reference'] = $translator->objectToArray(
                $tradeRecord->getReference(),
                $keys['reference']
            );
        }

        return $expression;
    }
}
