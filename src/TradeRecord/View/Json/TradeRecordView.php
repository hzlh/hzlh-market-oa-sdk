<?php
namespace ProductMarket\TradeRecord\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\TradeRecord\View\TradeRecordTrait;

class TradeRecordView extends JsonView implements IView
{
    use TradeRecordTrait;

    public function display() : void
    {
        $tradeRecord = $this->getTradeRecordData();
        $enterprise = $this->getEnterpriseData();

        $data = $tradeRecord;

        $data['payerEnterprise'] = $enterprise;

        $this->encode($data);
    }
}
