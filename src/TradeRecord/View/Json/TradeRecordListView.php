<?php
namespace ProductMarket\TradeRecord\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\TradeRecord\Translator\TradeRecordTranslator;

class TradeRecordListView extends JsonView implements IView
{
    private $tradeRecordTranslator;

    public function __construct(
        array $tradeRecord,
        int $count = 0
    ) {
        $this->tradeRecord = $tradeRecord;
        $this->count = $count;
        $this->tradeRecordTranslator = new TradeRecordTranslator();
        parent::__construct();
    }

    protected function getCount()
    {
        return $this->count;
    }

    protected function getTradeRecord()
    {
        return $this->tradeRecord;
    }

    protected function getTradeRecordTranslator() : TradeRecordTranslator
    {
        return $this->tradeRecordTranslator;
    }

    protected function getTradeRecordList()
    {
        $tradeRecords = $this->getTradeRecord();
        $translator = $this->getTradeRecordTranslator();

        $tradeRecordList = array();

        foreach ($tradeRecords as $tradeRecord) {
            $tradeRecordList[] = $translator->objectToArray(
                $tradeRecord,
                array(
                    'id',
                    'tradeTime',
                    'tradeType',
                    'tradeMoney',
                    'debtor',
                    'creditor',
                    'balance',
                    'status',
                    'comment',
                    'reference'=>[],
                    'memberAccount'=>[]
                )
            );
        }

        return $tradeRecordList;
    }

    public function display() : void
    {
        $list = $this->getTradeRecordList();
        
        $data = array(
            'list' => $list,
            'total' => $this->getCount()
        );

        $this->encode($data);
    }
}
