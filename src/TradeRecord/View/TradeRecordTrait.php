<?php
namespace ProductMarket\TradeRecord\View;

use Sdk\ProductMarket\TradeRecord\Model\TradeRecord;
use ProductMarket\TradeRecord\Translator\TradeRecordTranslator;

use Sdk\Enterprise\Model\Enterprise;
use Enterprise\Translator\EnterpriseTranslator;

trait TradeRecordTrait
{
    private $type;

    private $tradeRecord;

    private $tradeRecordTranslator;

    private $enterpriseTranslator;

    public function __construct(int $type, TradeRecord $tradeRecord, Enterprise $enterprise)
    {
        $this->type = $type;
        $this->tradeRecord = $tradeRecord;
        $this->enterprise = $enterprise;
        $this->tradeRecordTranslator = new TradeRecordTranslator();
        $this->enterpriseTranslator = new EnterpriseTranslator();
        parent::__construct();
    }

    protected function getTradeRecord() : TradeRecord
    {
        return $this->tradeRecord;
    }

    protected function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    protected function getTradeRecordTranslator() : TradeRecordTranslator
    {
        return $this->tradeRecordTranslator;
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return $this->enterpriseTranslator;
    }

    protected function getType() : int
    {
        return $this->type;
    }

    protected function getTradeRecordData()
    {
        $tradeRecordData = $this->getTradeRecord();
        $translator = $this->getTradeRecordTranslator();
        
        $tradeRecord = array();

        $tradeRecord = $translator->objectToArray($tradeRecordData);

        return $tradeRecord;
    }

    protected function getEnterpriseData()
    {
        $enterpriseData = $this->getEnterprise();
        $enterpriseTranslator = $this->getEnterpriseTranslator();
        
        $enterprise = array();

        $enterprise = $enterpriseTranslator->objectToArray($enterpriseData);

        return $enterprise;
    }
}
