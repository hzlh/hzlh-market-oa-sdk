<?php
namespace ProductMarket\MemberAccount\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\MemberAccount\Model\MemberAccount;
use Sdk\ProductMarket\MemberAccount\Model\NullMemberAccount;

use Member\Translator\MemberTranslator;

class MemberAccountTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $memberAccount = null)
    {
        unset($memberAccount);
        unset($expression);
        return NullMemberAccount::getInstance();
    }

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($memberAccount, array $keys = array())
    {
        if (!$memberAccount instanceof MemberAccount) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'member'=>[],
                'accountBalance',
                'frozenAccountBalance',
                'createTime',
                'updateTime',
                'statusTime',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($memberAccount->getId());
        }
        if (in_array('accountBalance', $keys)) {
            $expression['accountBalance'] = $memberAccount->getAccountBalance();
        }
        if (in_array('frozenAccountBalance', $keys)) {
            $expression['frozenAccountBalance'] = $memberAccount->getFrozenAccountBalance();
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $memberAccount->getStatusTime();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $memberAccount->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $memberAccount->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $memberAccount->getStatus();
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $memberAccount->getMember(),
                $keys['member']
            );
        }

        return $expression;
    }
}
