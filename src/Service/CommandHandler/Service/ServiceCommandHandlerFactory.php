<?php
namespace ProductMarket\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Framework\Classes\NullCommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'ProductMarket\Service\Command\Service\RejectServiceCommand'=>
        'ProductMarket\Service\CommandHandler\Service\RejectServiceCommandHandler',
        'ProductMarket\Service\Command\Service\ApproveServiceCommand'=>
        'ProductMarket\Service\CommandHandler\Service\ApproveServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : new NullCommandHandler();
    }
}
