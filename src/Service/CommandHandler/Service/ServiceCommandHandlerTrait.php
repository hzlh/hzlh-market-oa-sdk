<?php
namespace ProductMarket\Service\CommandHandler\Service;

use Sdk\ProductMarket\Service\Model\Service;
use Sdk\ProductMarket\Service\Repository\ServiceRepository;

trait ServiceCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }
    
    protected function fetchService(int $id) : Service
    {
        return $this->getRepository()->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }
}
