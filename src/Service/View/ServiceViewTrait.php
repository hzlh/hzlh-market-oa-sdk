<?php
namespace ProductMarket\Service\View;

use Sdk\ProductMarket\Service\Model\Service;
use ProductMarket\Service\Translator\ServiceTranslator;

use Tag\Translator\TagTranslator;

trait ServiceViewTrait
{
    private $service;

    private $tagList;

    private $translator;

    private $tagTranslator;

    public function __construct(Service $service, $tagList)
    {
        $this->service = $service;
        $this->tagList = $tagList;
        $this->translator = new ServiceTranslator();
        $this->tagTranslator =  new TagTranslator();
        parent::__construct();
    }

    protected function getService() : Service
    {
        return $this->service;
    }

    protected function getTranslator() : ServiceTranslator
    {
        return $this->translator;
    }
}
