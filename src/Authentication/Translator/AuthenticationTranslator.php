<?php
namespace ProductMarket\Authentication\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Authentication\Model\Authentication;
use Sdk\ProductMarket\Authentication\Model\NullAuthentication;

use Enterprise\Translator\EnterpriseTranslator;
use ProductMarket\ServiceCategory\Translator\ServiceCategoryTranslator;
use UserType\Translator\UserTypeTranslator;

class AuthenticationTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $authentication = null)
    {
        unset($authentication);
        unset($expression);

        return NullAuthentication::getInstance();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    protected function getServiceCategoryTranslator() : ServiceCategoryTranslator
    {
        return new ServiceCategoryTranslator();
    }

    protected function getUserTypeTranslator() : UserTypeTranslator
    {
        return new UserTypeTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($authentication, array $keys = array())
    {
        if (!$authentication instanceof Authentication) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'qualificationImage',
                'serviceCategory'=>[],
                'enterprise'=>[],
                'applyStatus',
                'rejectReason',
                'status',
                'createTime',
                'updateTime',
                'statusTime',
                'strategyType'=>[],
                'strategyTypeId',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($authentication->getId());
        }
        if (in_array('qualificationImage', $keys)) {
            $expression['qualificationImage'] = $authentication->getQualificationImage();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $authentication->getNumber();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = $authentication->getApplyStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $authentication->getRejectReason();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $authentication->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $authentication->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $authentication->getStatusTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $authentication->getStatus();
        }
        if (in_array('strategyTypeId', $keys)) {
            $expression['strategyTypeId'] = $authentication->getStrategyTypeId();
        }
        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $authentication->getEnterprise(),
                $keys['enterprise']
            );
        }
        if (isset($keys['serviceCategory'])) {
            $expression['serviceCategory'] = $this->getServiceCategoryTranslator()->objectToArray(
                $authentication->getServiceCategory(),
                $keys['serviceCategory']
            );
        }
        if (isset($keys['strategyType'])) {
            $expression['strategyType'] = $this->getUserTypeTranslator()->objectToArray(
                $authentication->getUserType(),
                $keys['strategyType']
            );
        }

        return $expression;
    }
}
