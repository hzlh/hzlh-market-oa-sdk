<?php
namespace ProductMarket\Authentication\CommandHandler\Authentication;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Framework\Classes\NullCommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;

class AuthenticationCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'ProductMarket\Authentication\Command\Authentication\RejectAuthenticationCommand'=>
            'ProductMarket\Authentication\CommandHandler\Authentication\RejectAuthenticationCommandHandler',
        'ProductMarket\Authentication\Command\Authentication\ApproveAuthenticationCommand'=>
        'ProductMarket\Authentication\CommandHandler\Authentication\ApproveAuthenticationCommandHandler',
        'ProductMarket\Authentication\Command\Authentication\RelationAuthenticationCommand'=>
        'ProductMarket\Authentication\CommandHandler\Authentication\RelationAuthenticationCommandHandler',
        'ProductMarket\Authentication\Command\Authentication\CancelRelationAuthenticationCommand'=>
        'ProductMarket\Authentication\CommandHandler\Authentication\CancelRelationAuthenticationCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : new NullCommandHandler();
    }
}
