<?php
namespace ProductMarket\Authentication\CommandHandler\Authentication;

use Sdk\ProductMarket\Authentication\Model\Authentication;
use Sdk\ProductMarket\Authentication\Model\NullAuthentication;
use Sdk\ProductMarket\Authentication\Repository\AuthenticationRepository;
use Sdk\UserType\Repository\UserTypeRepository;
use Sdk\UserType\Model\UserType;

trait AuthenticationCommandHandlerTrait
{
    private $repository;

    private $authentication;

    private $userTypeRepository;

    public function __construct()
    {
        $this->repository = new AuthenticationRepository();
        $this->authentication = new NullAuthentication();
        $this->userTypeRepository = new UserTypeRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
        unset($this->authentication);
    }

    protected function getAuthentication() : Authentication
    {
        return $this->authentication;
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    protected function getUserTypeRepository() : UserTypeRepository
    {
        return $this->userTypeRepository;
    }

    protected function fetchUserType($id) : UserType
    {
        return $this->getUserTypeRepository()
                    ->scenario(UserTypeRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);
    }

    protected function fetchAuthentication(int $id) : Authentication
    {
        return $this->getRepository()
                    ->scenario(AuthenticationRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);
    }
}
