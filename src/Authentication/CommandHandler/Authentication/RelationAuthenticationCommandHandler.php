<?php
namespace ProductMarket\Authentication\CommandHandler\Authentication;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\UserType\Model\UserType;

use ProductMarket\Authentication\Command\Authentication\RelationAuthenticationCommand;
use ProductMarket\Authentication\CommandHandler\Authentication\AuthenticationCommandHandlerTrait;

class RelationAuthenticationCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait, AuthenticationCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof RelationAuthenticationCommand)) {
            throw new \InvalidArgumentException;
        }

        $userType = $this->fetchUserType($command->strategyTypeId);
        $this->authentication = $this->fetchAuthentication($command->id);
        $this->authentication->setStrategyTypeId($command->strategyTypeId);
        $this->authentication->setUserType($userType);

        if ($this->authentication->relation()) {
            $this->logDriverError($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_RELATION'],
            ILogAble::CATEGORY['AUTHENTICATION'],
            $this->authentication->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->authentication->getNumber()
        );
    }
}
