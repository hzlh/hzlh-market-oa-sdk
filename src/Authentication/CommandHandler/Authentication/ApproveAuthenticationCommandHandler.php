<?php
namespace ProductMarket\Authentication\CommandHandler\Authentication;

use Marmot\Core;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\ApproveCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class ApproveAuthenticationCommandHandler extends ApproveCommandHandler implements ILogAble
{
    use AuthenticationCommandHandlerTrait, LogDriverCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchAuthentication($id);
    }

    public function getLog() : Log
    {

        return new Log(
            ILogAble::OPERATION['OPERATION_APPROVE'],
            ILogAble::CATEGORY['AUTHENTICATION'],
            $this->approveAble->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->approveAble->getNumber()
        );
    }
}
