<?php
namespace ProductMarket\Authentication\CommandHandler\Authentication;

use Marmot\Core;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\RejectCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class RejectAuthenticationCommandHandler extends RejectCommandHandler implements ILogAble
{
    use AuthenticationCommandHandlerTrait, LogDriverCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchAuthentication($id);
    }
    
    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_REJECT'],
            ILogAble::CATEGORY['AUTHENTICATION'],
            $this->rejectAble->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->rejectAble->getNumber()
        );
    }
}
