<?php
namespace ProductMarket\Authentication\CommandHandler\Authentication;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

use ProductMarket\Authentication\Command\Authentication\CancelRelationAuthenticationCommand;
use ProductMarket\Authentication\CommandHandler\Authentication\AuthenticationCommandHandlerTrait;

class CanCelRelationAuthenticationCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait, AuthenticationCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof CancelRelationAuthenticationCommand)) {
            throw new \InvalidArgumentException;
        }

        $authentication = $this->fetchAuthentication($command->id);

        if ($authentication->cancelRelation()) {
            $this->logDriverError($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_CANCEL_RELATION'],
            ILogAble::CATEGORY['AUTHENTICATION'],
            $this->authentication->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->authentication->getNumber()
        );
    }
}
