<?php
namespace ProductMarket\Authentication\View;

use Sdk\ProductMarket\Authentication\Model\Authentication;
use ProductMarket\Authentication\Translator\AuthenticationTranslator;

trait AuthenticationViewTrait
{
    private $authentication;

    private $translator;

    public function __construct(Authentication $authentication)
    {
        $this->authentication = $authentication;
        $this->translator = new AuthenticationTranslator();
        parent::__construct();
    }

    protected function getAuthentication() : Authentication
    {
        return $this->authentication;
    }

    protected function getTranslator() : AuthenticationTranslator
    {
        return $this->translator;
    }
}
