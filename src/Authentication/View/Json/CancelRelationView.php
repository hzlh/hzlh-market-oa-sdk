<?php
namespace ProductMarket\Authentication\View\Json;

use ProductMarket\Authentication\View\AuthenticationViewTrait;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;
use UserType\Translator\UserTypeTranslator;

class CancelRelationView extends JsonView implements IView
{
    use AuthenticationViewTrait;

    public function display() : void
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getAuthentication(),
            array(
                'strategyType'=>[
                    'id',
                    'name'
                ]
            )
        );

        $this->encode($data);
    }
}
