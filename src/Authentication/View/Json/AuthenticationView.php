<?php
namespace ProductMarket\Authentication\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\Authentication\View\AuthenticationViewTrait;

class AuthenticationView extends JsonView implements IView
{
    use AuthenticationViewTrait;

    public function display() : void
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getAuthentication()
        );

        $this->encode($data);
    }
}
