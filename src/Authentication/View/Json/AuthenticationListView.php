<?php
namespace ProductMarket\Authentication\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\Authentication\Translator\AuthenticationTranslator;

class AuthenticationListView extends JsonView implements IView
{
    private $authenticationList;

    private $count;

    private $translator;

    public function __construct(
        array $authenticationList,
        int $count
    ) {
        $this->authenticationList = $authenticationList;
        $this->count = $count;
        $this->translator = new AuthenticationTranslator();
        parent::__construct();
    }

    protected function getAuthenticationList() : array
    {
        return $this->authenticationList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : AuthenticationTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getTranslator();

        foreach ($this->getAuthenticationList() as $authentication) {
            $data[] = $translator->objectToArray(
                $authentication,
                array(
                    'id',
                    'number',
                    'serviceCategory'=>[],
                    'updateTime',
                    'applyStatus',
                    'enterprise'=>[],
                    // 'strategyType'=>[],
                    'strategyTypeId'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
