<?php
namespace ProductMarket\Authentication\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;
use UserType\Translator\UserTypeTranslator;

class RelationView extends JsonView implements IView
{
    private $userTypeList;

    private $userTypeTranslator;

    public function __construct($userTypeList)
    {
        parent::__construct();
        $this->userTypeList = $userTypeList;
        $this->userTypeTranslator = new UserTypeTranslator();
    }

    public function __destruct()
    {
        unset($this->userTypeList);
    }

    protected function getUserTypeList() : array
    {
        return $this->userTypeList;
    }

    protected function getUserTypeTranslator()
    {
        return $this->userTypeTranslator;
    }

    public function display() : void
    {
        $userTypeData = $this->getUserTypeList();
        $translator = $this->getUserTypeTranslator();
        $data = array();

        foreach ($userTypeData as $userTypeValue) {
            $data[] = $translator->objectToArray(
                $userTypeValue,
                array(
                    'id',
                    "name"
                )
            );
        }

        $this->encode($data);
    }
}
