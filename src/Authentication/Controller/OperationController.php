<?php
namespace ProductMarket\Authentication\Controller;

use ProductMarket\Authentication\Command\Authentication\RelationAuthenticationCommand;
use ProductMarket\Authentication\Command\Authentication\CancelRelationAuthenticationCommand;
use ProductMarket\Authentication\CommandHandler\Authentication\AuthenticationCommandHandlerFactory;
use ProductMarket\Authentication\View\Json\RelationView;
use ProductMarket\Authentication\View\Json\CancelRelationView;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalRoleCheckTrait;
use Common\Controller\Traits\OperatControllerTrait;
use Common\Controller\Interfaces\IOperatAbleController;

use Sdk\ProductMarket\Authentication\Repository\AuthenticationRepository;
use Sdk\Role\Model\IRoleAble;
use Sdk\UserType\Model\UserType;
use Sdk\UserType\Repository\UserTypeRepository;

class OperationController extends Controller implements IOperatAbleController
{
    use WebTrait, GlobalRoleCheckTrait, OperatControllerTrait;

    private $userTypeRepository;

    private $commandBus;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->userTypeRepository = new UserTypeRepository();
        $this->commandBus = new CommandBus(new AuthenticationCommandHandlerFactory());
        $this->repository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->userTypeRepository);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getUserTypeRepository() : UserTypeRepository
    {
        return $this->userTypeRepository;
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    public function relation(string $id) : bool
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['AUTHENTICATION'],
        //     IRoleAble::OPERATION['OPERATION_AUTHENTICATION_RELATION']
        // )
        // ) {
        //     return false;
        // }

        $id = marmot_decode($id);

        if (!$id) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        if ($this->getRequest()->isGetMethod()) {
            return $this->globalCheck() && $this->getRelationView();
        }

        return $this->globalCheck() && $this->getRelationAction($id);
    }

    protected function getRelationView() : bool
    {
        $sort = ['-updateTime'];

        $filter = array();
        $userTypeList = array();

        $filter['status'] = UserType::USERTYPE_STATUS['ENABLE'];

        list($count, $userTypeList) = $this->getUserTypeRepository()
                    ->scenario(UserTypeRepository::LIST_MODEL_UN)
                    ->search($filter, $sort, DEFAULT_PAGE, COMMON_SIZE);

        unset($count);

        $this->render(new RelationView($userTypeList));
        return true;
    }

    protected function getRelationAction($id) : bool
    {
        $strategyTypeId = $this->getRequest()->post('strategyTypeId', '');
        $strategyTypeId = marmot_decode($strategyTypeId);

        $command = new RelationAuthenticationCommand(
            $strategyTypeId,
            $id
        );

        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }

    public function cancelRelation(string $id) : bool
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['AUTHENTICATION'],
        //     IRoleAble::OPERATION['OPERATION_AUTHENTICATION_RELATION']
        // )
        // ) {
        //     return false;
        // }

        $id = marmot_decode($id);

        if (!$id) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        if ($this->getRequest()->isGetMethod()) {
            return $this->globalCheck() && $this->getCancelRelationView($id);
        }

        return $this->globalCheck() && $this->getCancelRelationAction($id);
    }

    protected function getCancelRelationView($id)
    {
        $authentication = $this->getRepository()
                    ->scenario(AuthenticationRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);

        if ($authentication instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new CancelRelationView($authentication, $id));
        return true;
    }

    protected function getCancelRelationAction($id)
    {
        $command = new CanCelRelationAuthenticationCommand(
            $id
        );

        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
}
