<?php
namespace ProductMarket\Authentication\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\ApproveControllerTrait;
use Common\Controller\Interfaces\IApproveAbleController;

use ProductMarket\Authentication\Command\Authentication\ApproveAuthenticationCommand;
use ProductMarket\Authentication\Command\Authentication\RejectAuthenticationCommand;
use ProductMarket\Authentication\CommandHandler\Authentication\AuthenticationCommandHandlerFactory;

use Sdk\Role\Model\IRoleAble;
use Common\Controller\Traits\GlobalRoleCheckTrait;

class ApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait, GlobalRoleCheckTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new AuthenticationCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function approveAction(int $id) : bool
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['AUTHENTICATION'],
        //     IRoleAble::OPERATION['OPERATION_REJECT']
        // )
        // ) {
        //     return false;
        // }

        return $this->getCommandBus()->send(new ApproveAuthenticationCommand($id));
    }

    protected function rejectAction(int $id) : bool
    {
        if (!$this->globalRoleCheck(
            IRoleAble::CATEGORY['AUTHENTICATION'],
            IRoleAble::OPERATION['OPERATION_REJECT']
        )
        ) {
            return false;
        }

        $request = $this->getRequest();

        $rejectReason = $request->post('rejectReason', '');

        $command = new RejectAuthenticationCommand(
            $rejectReason,
            $id
        );
        
        return $this->getCommandBus()->send($command);
    }
}
