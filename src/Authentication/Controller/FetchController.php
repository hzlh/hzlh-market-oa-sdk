<?php
namespace ProductMarket\Authentication\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Common\Model\IApplyAble;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use ProductMarket\Authentication\View\Json\AuthenticationView;
use ProductMarket\Authentication\View\Json\AuthenticationListView;
use Sdk\ProductMarket\Authentication\Repository\AuthenticationRepository;

use Sdk\Role\Model\IRoleAble;
use Common\Controller\Traits\GlobalRoleCheckTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalRoleCheckTrait;

    const SCENE = array(
        'PENDING' => 1,
        'APPROVE' => 2,
        'REJECT' => 3
    );

    const SEARCH = array(
        'TITLE' => 'enterpriseName',
        'NUMBER' => 'number'
    );

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new AuthenticationRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : AuthenticationRepository
    {
        return $this->repository;
    }

    protected function filterAction()
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['AUTHENTICATION'],
        //     IRoleAble::OPERATION['OPERATION_SHOW']
        // )
        // ) {
        //     return false;
        // }

        $search = $this->getRequest()->get('search', array());
        $searchType = $this->getRequest()->get('searchType', '');
        $scene = $this->getRequest()->get('scene', '');
        $scene = marmot_decode($scene);

        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($searchType, $search, $scene);

        $authenticationList = array();

        list($count, $authenticationList) =
            $this->getRepository()
            ->scenario(AuthenticationRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new AuthenticationListView($authenticationList, $count));
        return true;
    }

    protected function filterFormatChange($searchType, $search, $scene)
    {
        $sort = ['-updateTime'];
        $filter = array();

        if (!empty($search[0])) {
            if ($searchType == self::SEARCH['TITLE']) {
                $filter['enterpriseName'] = $search[0];
            }
            if ($searchType == self::SEARCH['NUMBER']) {
                $filter['number'] = $search[0];
            }
        }

        if ($scene == self::SCENE['PENDING']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'];
        }
        if ($scene == self::SCENE['APPROVE']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        }
        if ($scene == self::SCENE['REJECT']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['REJECT'];
        }

        return [$filter, $sort];
    }

    protected function fetchOneAction(int $id)
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['AUTHENTICATION'],
        //     IRoleAble::OPERATION['OPERATION_SHOW']
        // )
        // ) {
        //     return false;
        // }

        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $authentication = $this->getRepository()->scenario(AuthenticationRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        if ($authentication instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new AuthenticationView($authentication));
        return true;
    }
}
