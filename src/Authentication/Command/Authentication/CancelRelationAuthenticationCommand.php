<?php
namespace ProductMarket\Authentication\Command\Authentication;

use Marmot\Interfaces\ICommand;

class CancelRelationAuthenticationCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}
