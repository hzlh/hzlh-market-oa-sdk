<?php
namespace ProductMarket\ParentCategory\Controller;

use ProductMarket\ParentCategory\View\Json\ParentCategoryView;
use Sdk\ProductMarket\ServiceCategory\Repository\ParentCategoryRepository;
use ProductMarket\ParentCategory\Command\ParentCategory\AddParentCategoryCommand;
use ProductMarket\ParentCategory\Command\ParentCategory\EditParentCategoryCommand;
use ProductMarket\ParentCategory\CommandHandler\ParentCategory\ParentCategoryCommandHandlerFactory;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Interfaces\IOperatAbleController;
use Common\Controller\Traits\OperatControllerTrait;

use WidgetRules\Common\WidgetRules as CommonWidgetRules;

use Sdk\Role\Model\IRoleAble;
use Common\Controller\Traits\GlobalRoleCheckTrait;

class OperationController extends Controller implements IOperatAbleController
{
    use WebTrait, OperatControllerTrait, GlobalRoleCheckTrait;

    private $commandBus;

    private $commonWidgetRules;

    private $repository;
    
    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ParentCategoryCommandHandlerFactory());
        $this->commonWidgetRules = new CommonWidgetRules();
        $this->repository = new ParentCategoryRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getCommonWidgetRules() : CommonWidgetRules
    {
        return $this->commonWidgetRules;
    }

    protected function getRepository() : ParentCategoryRepository
    {
        return $this->repository;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function addAction()
    {
        $request = $this->getRequest();

        $name = $request->post('name', '');
      
        if ($this->validateCommonScenario(
            $name
        )) {
            $command = new AddParentCategoryCommand(
                $name
            );
      
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        $this->displayError();
        return false;
    }

    protected function validateCommonScenario(
        $name
    ) {
        return $this->getCommonWidgetRules()->name($name);
    }

    protected function editView(int $id) : bool
    {
        $parentCategory = $this->getRepository()
                    ->scenario(ParentCategoryRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);

        if ($parentCategory instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new ParentCategoryView($parentCategory));
        return true;
    }

    protected function editAction(int $id)
    {
        $request = $this->getRequest();

        $name = $request->post('name', '');
        
        if ($this->validateCommonScenario(
            $name
        )) {
            $command = new EditParentCategoryCommand(
                $name,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }
        $this->displayError();
        return false;
    }
}
