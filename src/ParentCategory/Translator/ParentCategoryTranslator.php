<?php
namespace ProductMarket\ParentCategory\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\ServiceCategory\Model\ParentCategory;
use Sdk\ProductMarket\ServiceCategory\Model\NullParentCategory;

class ParentCategoryTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $parentCategory = null)
    {
        unset($parentCategory);
        unset($expression);
        return NullParentCategory::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($parentCategory, array $keys = array())
    {
        if (!$parentCategory instanceof ParentCategory) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'createTime',
                'updateTime',
                'statusTime',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] =  marmot_encode($parentCategory->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $parentCategory->getName();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $parentCategory->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $parentCategory->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $parentCategory->getStatusTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $parentCategory->getStatus();
        }

        return $expression;
    }
}
