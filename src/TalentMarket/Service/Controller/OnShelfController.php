<?php


namespace ProductMarket\TalentMarket\Service\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\OnShelfControllerTrait;
use Common\Controller\Interfaces\IOnShelfAbleController;
use ProductMarket\TalentMarket\Service\Command\Service\OnShelfServiceCommand;
use ProductMarket\TalentMarket\Service\Command\Service\OffStockServiceCommand;
use ProductMarket\TalentMarket\Service\CommandHandler\Service\ServiceCommandHandlerFactory;


class OnShelfController extends Controller implements IOnShelfAbleController
{
    use WebTrait, OnShelfControllerTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function onShelfAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new OnShelfServiceCommand($id));
    }

    protected function offStockAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new OffStockServiceCommand($id));
    }
}