<?php

namespace ProductMarket\TalentMarket\Service\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;


use Common\Controller\Interfaces\IOperatAbleController;
use Common\Controller\Traits\GlobalRoleCheckTrait;
use Common\Controller\Traits\OperatControllerTrait;
use Common\Controller\Traits\ToolTrait;

use ProductMarket\TalentMarket\Service\Command\Service\EditServiceCommand;
use ProductMarket\TalentMarket\Service\Command\Service\AddServiceCommand;
use ProductMarket\TalentMarket\Service\CommandHandler\Service\ServiceCommandHandlerFactory;
use ProductMarket\TalentMarket\Service\WidgetRules\ServiceWidgetRules;
use ProductMarket\TalentMarket\ServiceCategory\Controller\OperatorControllerTrait;
use Sdk\TalentMarket\Service\Repository\ServiceRepository;
use WidgetRules\Common\WidgetRules as CommonWidgetRules;

use Sdk\Common\Model\IApplyAble;
use Sdk\Policy\Model\PolicyModelFactory;


class OperationController extends Controller implements IOperatAbleController
{
    use WebTrait, OperatControllerTrait, OperatorControllerTrait, GlobalRoleCheckTrait, ToolTrait;

    const PRE_ENTERPRISE = 1729;

    const ENTERPRISE = 5;

    private $commandBus;

    private $commonWidgetRules;

    private $serviceWidgetRules;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
        $this->commonWidgetRules = new CommonWidgetRules();
        $this->serviceWidgetRules = new ServiceWidgetRules();
        $this->repository = new ServiceRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getCommonWidgetRules() : CommonWidgetRules
    {
        return $this->commonWidgetRules;
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }

    protected function addView() : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function getServiceWidgetRules() : ServiceWidgetRules
    {
        return $this->serviceWidgetRules;
    }

    protected function validateOperationScenario(
        $title,
        $cover,
        $price,
        $contract,
        $serviceObjects,
        $detail,
        $serviceCategoryId
    ) : bool {
        return $this->getServiceWidgetRules()->title($title)
            && $this->getCommonWidgetRules()->image($cover)
            && (empty($price) ? true : $this->getServiceWidgetRules()->servicePrice($price))
            && (empty($contract) ? true : $this->getServiceWidgetRules()->contract($contract))
            && (empty($serviceObjects) ? true : $this->getServiceWidgetRules()->serviceObjects($serviceObjects))
            && (empty($detail) ? true :$this->getCommonWidgetRules()->detail($detail))
            && $this->getCommonWidgetRules()->formatNumeric($serviceCategoryId, 'serviceCategoryId');
    }

    protected function getCommonRequest()
    {
        $request = $this->getRequest();

        $requestData = array();
        $requestData['title'] = $request->post('title', '');
        $requestData['cover'] = $request->post('cover', array());
        $requestData['price'] = $request->post('price', array());
        $requestData['detail'] = $request->post('detail', array());
        $requestData['contract'] = $request->post('contract', array());

        $requestData['serviceObjects'] = [];
        $serviceObjects = $request->post('serviceObjects', '');
        if(!empty($serviceObjects)){
            $requestData['serviceObjects'] = $this->formatDacode($serviceObjects);
        }
    
        $requestData['tag'] = $request->post('tags', '');
        $requestData['tag'] = $this->tagsImplode($requestData['tag']);

        $serviceCategoryId = $request->post('serviceCategory', '');
        $requestData['serviceCategoryId'] = marmot_decode($serviceCategoryId);
        
        $requestData['enterpriseId'] = Core::$container->get('is_pro') ? self::PRE_ENTERPRISE : self::ENTERPRISE;
        
        return $requestData;
    }

    protected function addAction()
    {
        $request = $this->getCommonRequest();
       
        if ($this->validateOperationScenario(
            $request['title'],
            $request['cover'],
            $request['price'],
            $request['contract'],
            $request['serviceObjects'],
            $request['detail'],
            $request['serviceCategoryId']
        ) ) {
            $command = new AddServiceCommand(
                $request['title'],
                $request['tag'],
                $request['cover'],
                $request['serviceObjects'],
                $request['price'],
                $request['detail'],
                $request['contract'],
                $request['serviceCategoryId'],
                $request['enterpriseId'] ?? 0
            );
        
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        $request = $this->getCommonRequest();

        if ($this->validateOperationScenario(
            $request['title'],
            $request['cover'],
            $request['price'],
            $request['contract'],
            $request['serviceObjects'],
            $request['detail'],
            $request['serviceCategoryId']
        ) ) {
            $command = new EditServiceCommand(
                $request['title'],
                $request['tag'],
                $request['cover'],
                $request['serviceObjects'],
                $request['price'],
                $request['detail'],
                $request['contract'],
                $request['serviceCategoryId'],
                $request['enterpriseId'] ?? 0,
                $id
            );
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function tagsImplode($tags) : string
    {
        $tags = explode(',', $tags);
        foreach ($tags as $val) {
            $tagData[] = marmot_decode($val);
        }

        return implode(',', $tagData);
    }

    protected function getServiceDetail($id)
    {
        $service = $this->getServiceRepository()
            ->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        return $service;
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function getAuthenticationRepository() : AuthenticationRepository
    {
        return new AuthenticationRepository();
    }

    protected function getServiceRepository() : ServiceRepository
    {
        return new ServiceRepository();
    }

    protected function fetchServiceCategoryByIds($ids)
    {
        $serviceCategory = array();
        list($count, $serviceCategory) = $this->getServiceCategoryRepository()
            ->scenario(ServiceCategoryRepository::LIST_MODEL_UN)->fetchList($ids);
        unset($count);

        return $serviceCategory;
    }

    protected function fetchServiceObjects() : array
    {
        $applicableObjects = array();

        foreach (PolicyModelFactory::POLICY_APPLICABLE_OBJECT as $key => $applicableObject) {
            $applicableObjects[] = array(
                'id' => $key,
                'name' => $applicableObject
            );
        }

        return $applicableObjects;
    }

    protected function getServiceCategoryIds($enterpriseId)
    {
        $sort = ['-updateTime'];
        $filter = array();
        $filter['enterprise'] = $enterpriseId;
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];

        $authentications = array();
        list($count, $authentications) = $this->getAuthenticationRepository()
            ->scenario(AuthenticationRepository::LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, COMMON_SIZE);
        unset($count);

        $serviceCategoryIds = array();
        foreach ($authentications as $authentication) {
            $serviceCategoryIds[] = $authentication->getServiceCategory()->getId();
        }

        return $serviceCategoryIds;
    }
}