<?php
namespace ProductMarket\TalentMarket\Service\Command\Service;

use Marmot\Interfaces\ICommand;


class AddServiceCommand implements ICommand
{
    public $title;

    public $tag;

    public $cover;

    public $serviceObjects;

    public $price;

    public $detail;

    public $contract;

    public $serviceCategory;

    public $enterprise;

    public $id;

    public function __construct(
        string $title,
        string $tag,
        array $cover,
        array $serviceObjects,
        array $price,
        array $detail,
        array $contract,
        int $serviceCategory = 0,
        int $enterprise = 0,
        int $id = 0
    ) {
        $this->title = $title;
        $this->tag = $tag;
        $this->cover = $cover;
        $this->serviceObjects = $serviceObjects;
        $this->price = $price;
        $this->detail = $detail;
        $this->contract = $contract;
        $this->serviceCategory = $serviceCategory;
        $this->enterprise = $enterprise;
        $this->id = $id;
    }
}