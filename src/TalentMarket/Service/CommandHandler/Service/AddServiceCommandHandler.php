<?php
namespace  ProductMarket\TalentMarket\Service\CommandHandler\Service;

use Marmot\Core;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use ProductMarket\TalentMarket\Service\Command\Service\AddServiceCommand;
use Sdk\Common\Model\IApplyAble;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Repository\EnterpriseRepository;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;
use Sdk\TalentMarket\Service\Model\Service;
use Sdk\TalentMarket\ServiceCategory\Repository\ServiceCategoryRepository;

class AddServiceCommandHandler implements ICommandHandler,ILogAble
{
    use ServiceCommandHandlerTrait, LogDriverCommandHandlerTrait;

    private $service;

    public function __construct()
    {
        $this->service = new Service();
    }

    protected function fetchIApplyObject() : IApplyAble
    {
        return $this->service;
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(AddServiceCommand $command)
    {
        $service = $this->fetchIApplyObject();

        $enterprise = $this->fetchEnterprise($command->enterprise);
      
        $serviceCategory = $this->fetchServiceCategory($command->serviceCategory);

        $this->service->setEnterprise($enterprise);
        $this->service->setTitle($command->title);
        $this->service->setCover($command->cover);
        $this->service->setServiceObjects($command->serviceObjects);
        $this->service->setPrice($command->price);
        $this->service->setDetail($command->detail);
        $this->service->setContract($command->contract);
        $this->service->setServiceCategory($serviceCategory);
        $this->service->setTag($command->tag);

        if ($this->service->add()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function fetchServiceCategory($id)
    {
        return $this->getServiceCategoryRepository()
            ->scenario(ServiceCategoryRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return new EnterpriseRepository();
    }

    protected function fetchEnterprise($id) : Enterprise
    {
        return $this->getEnterpriseRepository()->scenario(EnterpriseRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_ADD'],
            ILogAble::CATEGORY['SERVICE'],
            $this->service->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->service->getNumber()
        );
    }
}
