<?php
namespace  ProductMarket\TalentMarket\Service\CommandHandler\Service;

use Sdk\TalentMarket\Service\Model\Service;
use Sdk\TalentMarket\Service\Repository\ServiceRepository;

trait ServiceCommandHandlerTrait
{
    protected function getRepository() : ServiceRepository
    {
        return new ServiceRepository();
    }
    
    protected function fetchService(int $id) : Service
    {
        return $this->getRepository()->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }
}
