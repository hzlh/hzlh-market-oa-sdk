<?php


namespace ProductMarket\TalentMarket\Service\CommandHandler\Service;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Sdk\Common\Utils\LogDriverCommandHandlerTrait;
use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;

use ProductMarket\TalentMarket\Service\Command\Service\OffStockServiceCommand;

class OffStockServiceCommandHandler implements ICommandHandler, ILogAble
{
    use ServiceCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof OffStockServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->service = $this->fetchService($command->id);

        if ($this->service->offStock()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_OFF_STOCK'],
            ILogAble::CATEGORY['SERVICE'],
            $this->service->getId(),
            Log::TYPE['MEMBER'],
            Core::$container->get('crew'),
            $this->service->getNumber(),
            0
        );
    }
}