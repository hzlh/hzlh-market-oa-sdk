<?php
namespace  ProductMarket\TalentMarket\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Framework\Classes\NullCommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'ProductMarket\TalentMarket\Service\Command\Service\RejectServiceCommand'=>
        'ProductMarket\TalentMarket\Service\CommandHandler\Service\RejectServiceCommandHandler',
        'ProductMarket\TalentMarket\Service\Command\Service\ApproveServiceCommand'=>
        'ProductMarket\TalentMarket\Service\CommandHandler\Service\ApproveServiceCommandHandler',
        'ProductMarket\TalentMarket\Service\Command\Service\AddServiceCommand'=>
            'ProductMarket\TalentMarket\Service\CommandHandler\Service\AddServiceCommandHandler',
        'ProductMarket\TalentMarket\Service\Command\Service\EditServiceCommand'=>
            'ProductMarket\TalentMarket\Service\CommandHandler\Service\EditServiceCommandHandler',
        'ProductMarket\TalentMarket\Service\Command\Service\OnShelfServiceCommand'=>
            'ProductMarket\TalentMarket\Service\CommandHandler\Service\OnShelfServiceCommandHandler',
        'ProductMarket\TalentMarket\Service\Command\Service\OffStockServiceCommand'=>
            'ProductMarket\TalentMarket\Service\CommandHandler\Service\OffStockServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : new NullCommandHandler();
    }
}
