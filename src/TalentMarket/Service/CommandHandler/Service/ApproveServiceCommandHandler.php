<?php
namespace  ProductMarket\TalentMarket\Service\CommandHandler\Service;

use Marmot\Core;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\ApproveCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class ApproveServiceCommandHandler extends ApproveCommandHandler implements ILogAble
{
    use ServiceCommandHandlerTrait, LogDriverCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchService($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_APPROVE'],
            ILogAble::CATEGORY['SERVICE'],
            $this->approveAble->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->approveAble->getNumber()
        );
    }
}
