<?php
namespace  ProductMarket\TalentMarket\Service\CommandHandler\Service;

use Marmot\Core;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\RejectCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class RejectServiceCommandHandler extends RejectCommandHandler implements ILogAble
{
    use ServiceCommandHandlerTrait, LogDriverCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchService($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_REJECT'],
            ILogAble::CATEGORY['SERVICE'],
            $this->rejectAble->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->rejectAble->getNumber()
        );
    }
}
