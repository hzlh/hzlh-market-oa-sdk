<?php
namespace  ProductMarket\TalentMarket\Service\CommandHandler\Service;

use Marmot\Core;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use ProductMarket\TalentMarket\Service\Command\Service\EditServiceCommand;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\RejectCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;
use Sdk\TalentMarket\Service\Model\Service;
use Sdk\TalentMarket\ServiceCategory\Repository\ServiceCategoryRepository;

class EditServiceCommandHandler implements ICommandHandler,ILogAble
{
    use ServiceCommandHandlerTrait, LogDriverCommandHandlerTrait;

    private $service;

    public function __construct()
    {
        $this->service = new Service();
    }

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(EditServiceCommand $command)
    {
        $this->service = $this->fetchService($command->id);
        $serviceCategory = $this->fetchServiceCategory($command->serviceCategory);

        $this->service->setTitle($command->title);
        $this->service->setCover($command->cover);
        $this->service->setServiceObjects($command->serviceObjects);
        $this->service->setPrice($command->price);
        $this->service->setDetail($command->detail);
        $this->service->setContract($command->contract);
        $this->service->setServiceCategory($serviceCategory);
        $this->service->setTag($command->tag);

        if ($this->service->edit()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    protected function getServiceCategoryRepository() : ServiceCategoryRepository
    {
        return new ServiceCategoryRepository();
    }

    protected function fetchServiceCategory($id)
    {
        return $this->getServiceCategoryRepository()
            ->scenario(ServiceCategoryRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_EDIT'],
            ILogAble::CATEGORY['SERVICE'],
            $this->service->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->service->getNumber()
        );
    }
}
