<?php
namespace  ProductMarket\TalentMarket\Service\Translator;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Classes\Filter;

use Sdk\TalentMarket\Service\Model\Service;
use Sdk\TalentMarket\Service\Model\NullService;

use Enterprise\Translator\EnterpriseTranslator;
use ProductMarket\TalentMarket\ServiceCategory\Translator\ServiceCategoryTranslator;
use Policy\Translator\PolicyCategoryTranslator;

class ServiceTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $service = null)
    {
        unset($service);
        unset($expression);
        return NullService::getInstance();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    protected function getServiceCategoryTranslator() : ServiceCategoryTranslator
    {
        return new ServiceCategoryTranslator();
    }

    protected function getPolicyCategoryTranslator() : PolicyCategoryTranslator
    {
        return new PolicyCategoryTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($service, array $keys = array())
    {
        if (!$service instanceof Service) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'title',
                'cover',
                'serviceObjects'=>[],
                'minPrice',
                'maxPrice',
                'enterprise'=>[],
                'serviceCategory'=>['id', 'name'],
                'price',
                'detail',
                'contract',
                'volume',
                'attentionDegree',
                'applyStatus',
                'rejectReason',
                'createTime',
                'updateTime',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($service->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $service->getTitle();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $service->getNumber();
        }
        if (in_array('cover', $keys)) {
            $expression['cover'] = $service->getCover();
        }
        if (isset($keys['serviceObjects'])) {
            $expression['serviceObjects'] = $this->serviceObjects($keys, $service);
        }
        if (in_array('minPrice', $keys)) {
            $expression['minPrice'] = $service->getMinPrice();
        }
        if (in_array('maxPrice', $keys)) {
            $expression['maxPrice'] = $service->getMaxPrice();
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $service->getPrice();
        }
        if (in_array('detail', $keys)) {
            $expression['detail'] = Filter::dhtmlspecialchars($service->getDetail());
        }
        if (in_array('contract', $keys)) {
            $expression['contract'] = $service->getContract();
        }
        if (in_array('volume', $keys)) {
            $expression['volume'] = $service->getVolume();
        }
        if (in_array('attentionDegree', $keys)) {
            $expression['attentionDegree'] = $service->getAttentionDegree();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = $service->getApplyStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $service->getRejectReason();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $service->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $service->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $service->getStatus();
        }
        if (isset($keys['enterprise'])) {
            $expression['enterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $service->getEnterprise(),
                $keys['enterprise']
            );
        }
        if (isset($keys['serviceCategory'])) {
            $expression['serviceCategory'] = $this->getServiceCategoryTranslator()->objectToArray(
                $service->getServiceCategory(),
                $keys['serviceCategory']
            );
        }

        return $expression;
    }

    private function serviceObjects($keys, $service)
    {
        $serviceObjects = $service->getServiceObjects();
        
        $serviceObjectArray = array();
        foreach ($serviceObjects as $serviceObject) {
            $serviceObjectArray[] = $this->getPolicyCategoryTranslator()->objectToArray(
                $serviceObject,
                $keys['serviceObjects']
            );
        }

        return $serviceObjectArray;
    }
}
