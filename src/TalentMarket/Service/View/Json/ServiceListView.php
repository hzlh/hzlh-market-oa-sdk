<?php
namespace  ProductMarket\TalentMarket\Service\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\TalentMarket\Service\Translator\ServiceTranslator;

class ServiceListView extends JsonView implements IView
{
    private $serviceList;

    private $count;

    private $translator;

    public function __construct(
        array $serviceList,
        int $count
    ) {
        $this->serviceList = $serviceList;
        $this->count = $count;
        $this->translator = new ServiceTranslator();
        parent::__construct();
    }

    protected function getServiceList() : array
    {
        return $this->serviceList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getServiceTranslator() : ServiceTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getServiceTranslator();

        foreach ($this->getServiceList() as $service) {
            $data[] = $translator->objectToArray(
                $service,
                array(
                    'id',
                    'number',
                    'title',
                    'cover',
                    'updateTime',
                    'applyStatus',
                    'status',
                    'minPrice',
                    'volume',
                    'enterprise'=>['id','name','contactsCellphone','logo'],
                    'serviceCategory'=>['id','name']
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
