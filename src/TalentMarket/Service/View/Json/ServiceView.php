<?php
namespace  ProductMarket\TalentMarket\Service\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\TalentMarket\Service\View\ServiceViewTrait;

use Tag\Translator\TagTranslatorTrait;

class ServiceView extends JsonView implements IView
{
    use ServiceViewTrait,  TagTranslatorTrait;
    
    public function display() : void
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getService()
        );

        $tag = $this->tagObjectToArray();
        $data['tag'] = !empty($tag) ? $tag : [];

        $this->encode($data);
    }
}
