<?php
namespace  ProductMarket\TalentMarket\ServiceRequirement\Translator;

use Marmot\Framework\Classes\Filter;
use Marmot\Interfaces\ITranslator;
use Member\Translator\MemberTranslator;

use Sdk\TalentMarket\ServiceRequirement\Model\NullServiceRequirement;
use Sdk\TalentMarket\ServiceRequirement\Model\ServiceRequirement;
use ProductMarket\TalentMarket\ServiceCategory\Translator\ServiceCategoryTranslator;

class ServiceRequirementTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $serviceRequirement = null)
    {
        unset($serviceRequirement);
        unset($expression);
        return NullServiceRequirement::getInstance();
    }

    protected function getMemberTranslator(): MemberTranslator
    {
        return new MemberTranslator();
    }

    protected function getServiceCategoryTranslator(): ServiceCategoryTranslator
    {
        return new ServiceCategoryTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($serviceRequirement, array $keys = array())
    {
        if (!$serviceRequirement instanceof ServiceRequirement) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'number',
                'detail',
                'minPrice',
                'maxPrice',
                'validityStartTime',
                'validityEndTime',
                'contactName',
                'contactPhone',
                'member' => [],
                'serviceCategory' => [],
                'createTime',
                'updateTime',
                'applyStatus',
                'rejectReason',
                'status'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($serviceRequirement->getId());
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $serviceRequirement->getTitle();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $serviceRequirement->getNumber();
        }
        if (in_array('detail', $keys)) {
            $expression['detail'] = Filter::dhtmlspecialchars($serviceRequirement->getDetail());
        }
        if (in_array('minPrice', $keys)) {
            $expression['minPrice'] = $serviceRequirement->getMinPrice();
        }
        if (in_array('maxPrice', $keys)) {
            $expression['maxPrice'] = $serviceRequirement->getMaxPrice();
        }
        if (in_array('validityStartTime', $keys)) {
            $expression['validityStartTime'] = $serviceRequirement->getValidityStartTime();
        }
        if (in_array('validityEndTime', $keys)) {
            $expression['validityEndTime'] = $serviceRequirement->getValidityEndTime();
        }
        if (in_array('contactName', $keys)) {
            $expression['contactName'] = $serviceRequirement->getContactName();
        }
        if (in_array('contactPhone', $keys)) {
            $expression['contactPhone'] = $serviceRequirement->getContactPhone();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['applyStatus'] = $serviceRequirement->getApplyStatus();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['rejectReason'] = $serviceRequirement->getRejectReason();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $serviceRequirement->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $serviceRequirement->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $serviceRequirement->getStatus();
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $serviceRequirement->getMember(),
                $keys['member']
            );
        }

        if (isset($keys['serviceCategory'])) {
            $expression['serviceCategory'] = $this->getServiceCategoryTranslator()->objectToArray(
                $serviceRequirement->getServiceCategory(),
                $keys['serviceCategory']
            );
        }

        return $expression;
    }
}
