<?php
namespace  ProductMarket\TalentMarket\ServiceRequirement\CommandHandler\ServiceRequirement;

use Sdk\TalentMarket\ServiceRequirement\Model\ServiceRequirement;
use Sdk\TalentMarket\ServiceRequirement\Repository\ServiceRequirementRepository;

trait ServiceRequirementCommandHandlerTrait
{
    private $repository;
    
    public function __construct()
    {
        $this->repository = new ServiceRequirementRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ServiceRequirementRepository
    {
        return $this->repository;
    }
    
    protected function fetchServiceRequirement(int $id) : ServiceRequirement
    {
        return $this->getRepository()->scenario(ServiceRequirementRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }
}
