<?php
namespace  ProductMarket\TalentMarket\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Core;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\RejectCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class RejectServiceRequirementCommandHandler extends RejectCommandHandler implements ILogAble
{
    use ServiceRequirementCommandHandlerTrait, LogDriverCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchServiceRequirement($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_REJECT'],
            ILogAble::CATEGORY['SERVICE_REQUIREMENT'],
            $this->rejectAble->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->rejectAble->getNumber()
        );
    }
}
