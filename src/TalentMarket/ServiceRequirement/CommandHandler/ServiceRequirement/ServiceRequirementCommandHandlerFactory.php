<?php
namespace  ProductMarket\TalentMarket\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Framework\Classes\NullCommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;

class ServiceRequirementCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'ProductMarket\TalentMarket\ServiceRequirement\Command\ServiceRequirement\RejectServiceRequirementCommand'=>
        'ProductMarket\TalentMarket\ServiceRequirement\CommandHandler\ServiceRequirement\RejectServiceRequirementCommandHandler',
        'ProductMarket\TalentMarket\ServiceRequirement\Command\ServiceRequirement\ApproveServiceRequirementCommand'=>
        'ProductMarket\TalentMarket\ServiceRequirement\CommandHandler\ServiceRequirement\ApproveServiceRequirementCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : new NullCommandHandler();
    }
}
