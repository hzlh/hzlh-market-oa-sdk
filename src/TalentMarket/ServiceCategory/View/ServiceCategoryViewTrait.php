<?php
namespace  ProductMarket\TalentMarket\ServiceCategory\View;

use Sdk\ProductMarket\ServiceCategory\Model\ServiceCategory;
use ProductMarket\ServiceCategory\Translator\ServiceCategoryTranslator;

trait ServiceCategoryViewTrait
{
    private $serviceCategory;

    private $translator;

    public function __construct(ServiceCategory $serviceCategory)
    {
        $this->serviceCategory = $serviceCategory;
        $this->translator = new ServiceCategoryTranslator();
        parent::__construct();
    }

    protected function getServiceCategory() : ServiceCategory
    {
        return $this->serviceCategory;
    }

    protected function getTranslator() : ServiceCategoryTranslator
    {
        return $this->translator;
    }
}
