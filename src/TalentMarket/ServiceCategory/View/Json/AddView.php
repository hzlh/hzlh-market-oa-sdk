<?php
namespace  ProductMarket\TalentMarket\ServiceCategory\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\ParentCategory\Translator\ParentCategoryTranslator;

class AddView extends JsonView implements IView
{
    private $parentCategories;

    private $parentCategoryTranslator;

    public function __construct(array $parentCategories)
    {
        $this->parentCategories = $parentCategories;
        $this->parentCategoryTranslator = new ParentCategoryTranslator();
        parent::__construct();
    }

    protected function getParentCategories() : array
    {
        return $this->parentCategories;
    }

    protected function getParentCategoryTranslator() : ParentCategoryTranslator
    {
        return $this->parentCategoryTranslator;
    }

    public function display() : void
    {
        $parentCategories = $this->getParentCategories();

        foreach ($parentCategories as $parentCategory) {
            $parentCategoryArray[] = $this->getParentCategoryTranslator()->ObjectToArray(
                $parentCategory,
                array('id', 'name')
            );
        }

        $data['parentCategories'] = $parentCategoryArray;

        $this->encode($data);
    }
}
