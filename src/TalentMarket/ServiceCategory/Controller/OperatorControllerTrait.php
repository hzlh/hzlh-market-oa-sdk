<?php
namespace  ProductMarket\TalentMarket\ServiceCategory\Controller;

use Sdk\ProductMarket\ServiceCategory\Repository\ParentCategoryRepository;

trait OperatorControllerTrait
{
    protected function getParentCategoryRepository() : ParentCategoryRepository
    {
        return new ParentCategoryRepository();
    }

    public function fetchParentCategories() : array
    {
        $parentCategories = array();

        $filter = array();
        $sort = ['-updateTime'];
        $page = DEFAULT_PAGE;
        $size = MAX_SIZE;
        
        list($count, $parentCategories) = $this->getParentCategoryRepository()
                            ->scenario(ParentCategoryRepository::LIST_MODEL_UN)
                            ->search($filter, $sort, $page, $size);
        unset($count);
        
        return $parentCategories;
    }
}
