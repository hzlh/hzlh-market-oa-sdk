<?php
namespace  ProductMarket\TalentMarket\ServiceCategory\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\EnableControllerTrait;
use Common\Controller\Interfaces\IEnableAbleController;

use ProductMarket\ServiceCategory\Command\ServiceCategory\EnableServiceCategoryCommand;
use ProductMarket\ServiceCategory\Command\ServiceCategory\DisableServiceCategoryCommand;
use ProductMarket\ServiceCategory\CommandHandler\ServiceCategory\ServiceCategoryCommandHandlerFactory;

use Sdk\Role\Model\IRoleAble;
use Common\Controller\Traits\GlobalRoleCheckTrait;

class EnableController extends Controller implements IEnableAbleController
{
    use WebTrait, EnableControllerTrait, GlobalRoleCheckTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceCategoryCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function enableAction(int $id) : bool
    {
        if (!$this->globalRoleCheck(
            IRoleAble::CATEGORY['PARENT_CATEGORY'],
            IRoleAble::OPERATION['OPERATION_ENABLE']
        )
        ) {
            return false;
        }
        return $this->getCommandBus()->send(new EnableServiceCategoryCommand($id));
    }

    protected function disableAction(int $id) : bool
    {
        if (!$this->globalRoleCheck(
            IRoleAble::CATEGORY['PARENT_CATEGORY'],
            IRoleAble::OPERATION['OPERATION_ENABLE']
        )
        ) {
            return false;
        }
        return $this->getCommandBus()->send(new DisableServiceCategoryCommand($id));
    }
}
