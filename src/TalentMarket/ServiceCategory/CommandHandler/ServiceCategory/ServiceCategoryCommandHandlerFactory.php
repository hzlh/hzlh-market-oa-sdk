<?php
namespace  ProductMarket\TalentMarket\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCategoryCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'ProductMarket\ServiceCategory\Command\ServiceCategory\AddServiceCategoryCommand'=>
        'ProductMarket\ServiceCategory\CommandHandler\ServiceCategory\AddServiceCategoryCommandHandler',
        'ProductMarket\ServiceCategory\Command\ServiceCategory\EditServiceCategoryCommand'=>
        'ProductMarket\ServiceCategory\CommandHandler\ServiceCategory\EditServiceCategoryCommandHandler',
        'ProductMarket\ServiceCategory\Command\ServiceCategory\DisableServiceCategoryCommand'=>
        'ProductMarket\ServiceCategory\CommandHandler\ServiceCategory\DisableServiceCategoryCommandHandler',
        'ProductMarket\ServiceCategory\Command\ServiceCategory\EnableServiceCategoryCommand'=>
        'ProductMarket\ServiceCategory\CommandHandler\ServiceCategory\EnableServiceCategoryCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
