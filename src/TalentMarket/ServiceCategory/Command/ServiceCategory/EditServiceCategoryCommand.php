<?php
namespace  ProductMarket\TalentMarket\ServiceCategory\Command\ServiceCategory;

use Marmot\Interfaces\ICommand;

class EditServiceCategoryCommand implements ICommand
{
    public $name;

    public $isQualification;

    public $isEnterpriseVerify;

    public $qualificationName;

    public $commission;

    public $status;

    public $id;

    public function __construct(
        string $name,
        string $qualificationName,
        int $isEnterpriseVerify,
        int $isQualification = 0,
        float $commission = 0,
        int $status = 0,
        int $id = 0
    ) {
        $this->name = $name;
        $this->qualificationName = $qualificationName;
        $this->isEnterpriseVerify = $isEnterpriseVerify;
        $this->isQualification = $isQualification;
        $this->commission = $commission;
        $this->status = $status;
        $this->id = $id;
    }
}
