<?php
namespace  ProductMarket\TalentMarket\ParentCategory\CommandHandler\ParentCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ParentCategoryCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'ProductMarket\ParentCategory\Command\ParentCategory\AddParentCategoryCommand'=>
        'ProductMarket\ParentCategory\CommandHandler\ParentCategory\AddParentCategoryCommandHandler',
        'ProductMarket\ParentCategory\Command\ParentCategory\EditParentCategoryCommand'=>
        'ProductMarket\ParentCategory\CommandHandler\ParentCategory\EditParentCategoryCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
