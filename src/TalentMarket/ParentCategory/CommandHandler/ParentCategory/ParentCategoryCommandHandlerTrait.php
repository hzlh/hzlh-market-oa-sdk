<?php
namespace  ProductMarket\TalentMarket\ParentCategory\CommandHandler\ParentCategory;

use Sdk\ProductMarket\ServiceCategory\Model\ParentCategory;
use Sdk\ProductMarket\ServiceCategory\Model\NullParentCategory;
use Sdk\ProductMarket\ServiceCategory\Repository\ParentCategoryRepository;

trait ParentCategoryCommandHandlerTrait
{
    private $parentCategory;

    private $repository;
    
    public function __construct()
    {
        $this->parentCategory = new NullParentCategory();
        $this->repository = new ParentCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->parentCategory);
        unset($this->repository);
    }

    protected function getParentCategory() : ParentCategory
    {
        return $this->parentCategory;
    }
    
    protected function getRepository() : ParentCategoryRepository
    {
        return $this->repository;
    }
    
    protected function fetchParentCategory(int $id) : ParentCategory
    {
        return $this->getRepository()->fetchOne($id);
    }
}
