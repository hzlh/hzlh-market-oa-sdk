<?php
namespace  ProductMarket\TalentMarket\ParentCategory\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use ProductMarket\TalentMarket\ParentCategory\View\Json\ParentCategoryView;
use ProductMarket\TalentMarket\ParentCategory\View\Json\ParentCategoryListView;
use Sdk\TalentMarket\ServiceCategory\Repository\ParentCategoryRepository;
use Sdk\Common\Model\IEnableAble;

use Common\Controller\Traits\GlobalRoleCheckTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalRoleCheckTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ParentCategoryRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ParentCategoryRepository
    {
        return $this->repository;
    }

    protected function filterAction()
    {
        $search = $this->getRequest()->get('search', array());

        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($search);

        $parentCategoryList = array();

        list($count, $parentCategoryList) =
            $this->getRepository()
            ->scenario(ParentCategoryRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);

        $this->render(new ParentCategoryListView($parentCategoryList, $count));
        return true;
    }

    protected function filterFormatChange($search)
    {
        $sort = ['-updateTime'];
        $filter = array();

        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        if (!empty($search[0])) {
            $filter['name'] = $search[0];
        }

        return [$filter, $sort];
    }

    protected function fetchOneAction(int $id)
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $parentCategory = $this->getRepository()
                    ->scenario(ParentCategoryRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);

        if ($parentCategory instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new ParentCategoryView($parentCategory));
        return true;
    }
}
