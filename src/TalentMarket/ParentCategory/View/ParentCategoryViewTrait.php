<?php
namespace  ProductMarket\TalentMarket\ParentCategory\View;

use Sdk\ProductMarket\TalentMarket\ServiceCategory\Model\ParentCategory;
use ProductMarket\TalentMarket\ParentCategory\Translator\ParentCategoryTranslator;

trait ParentCategoryViewTrait
{
    private $parentCategory;

    private $translator;

    public function __construct(ParentCategory $parentCategory)
    {
        $this->parentCategory = $parentCategory;
        $this->translator = new ParentCategoryTranslator();
        parent::__construct();
    }

    protected function getParentCategory() : ParentCategory
    {
        return $this->parentCategory;
    }

    protected function getTranslator() : ParentCategoryTranslator
    {
        return $this->translator;
    }
}
