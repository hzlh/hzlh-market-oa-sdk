<?php
namespace  ProductMarket\TalentMarket\ParentCategory\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\TalentMarket\ParentCategory\Translator\ParentCategoryTranslator;

class ParentCategoryListView extends JsonView implements IView
{
    private $parentCategoryList;

    private $count;

    private $translator;

    public function __construct(
        array $parentCategoryList,
        int $count
    ) {
        $this->parentCategoryList = $parentCategoryList;
        $this->count = $count;
        $this->translator = new ParentCategoryTranslator();
        parent::__construct();
    }

    protected function getParentCategoryList() : array
    {
        return $this->parentCategoryList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : ParentCategoryTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();

        $translator = $this->getTranslator();

        foreach ($this->getParentCategoryList() as $parentCategory) {
            $data[] = $translator->objectToArray(
                $parentCategory,
                array(
                    'id',
                    'name',
                    'updateTime',
                    'status'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
