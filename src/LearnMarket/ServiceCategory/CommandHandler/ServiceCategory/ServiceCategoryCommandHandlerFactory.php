<?php
namespace  ProductMarket\LearnMarket\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ServiceCategoryCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'ProductMarket\LearnMarket\ServiceCategory\Command\ServiceCategory\AddServiceCategoryCommand'=>
        'ProductMarket\LearnMarket\ServiceCategory\CommandHandler\ServiceCategory\AddServiceCategoryCommandHandler',
        'ProductMarket\LearnMarket\ServiceCategory\Command\ServiceCategory\EditServiceCategoryCommand'=>
        'ProductMarket\LearnMarket\ServiceCategory\CommandHandler\ServiceCategory\EditServiceCategoryCommandHandler',
        'ProductMarket\LearnMarket\ServiceCategory\Command\ServiceCategory\DisableServiceCategoryCommand'=>
        'ProductMarket\LearnMarket\ServiceCategory\CommandHandler\ServiceCategory\DisableServiceCategoryCommandHandler',
        'ProductMarket\LearnMarket\ServiceCategory\Command\ServiceCategory\EnableServiceCategoryCommand'=>
        'ProductMarket\LearnMarket\ServiceCategory\CommandHandler\ServiceCategory\EnableServiceCategoryCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}
