<?php
namespace  ProductMarket\LearnMarket\ServiceCategory\CommandHandler\ServiceCategory;

use Sdk\LearnMarket\ServiceCategory\Model\ServiceCategory;
use Sdk\LearnMarket\ServiceCategory\Model\NullServiceCategory;
use Sdk\LearnMarket\ServiceCategory\Repository\ServiceCategoryRepository;

trait ServiceCategoryCommandHandlerTrait
{
    private $serviceCategory;

    private $repository;
    
    public function __construct()
    {
        $this->serviceCategory = new NullServiceCategory();
        $this->repository = new ServiceCategoryRepository();
    }

    public function __destruct()
    {
        unset($this->serviceCategory);
        unset($this->repository);
    }

    protected function getServiceCategory() : ServiceCategory
    {
        return $this->serviceCategory;
    }
    
    protected function getRepository() : ServiceCategoryRepository
    {
        return $this->repository;
    }
    
    protected function fetchServiceCategory(int $id) : ServiceCategory
    {
        return $this->getRepository()->fetchOne($id);
    }
}
