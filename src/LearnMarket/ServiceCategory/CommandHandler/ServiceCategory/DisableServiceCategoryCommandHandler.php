<?php
namespace  ProductMarket\LearnMarket\ServiceCategory\CommandHandler\ServiceCategory;

use Marmot\Core;

use Sdk\Common\Model\IEnableAble;
use Sdk\Common\CommandHandler\DisableCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class DisableServiceCategoryCommandHandler extends DisableCommandHandler implements ILogAble
{
    use ServiceCategoryCommandHandlerTrait, LogDriverCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchServiceCategory($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_DISABLE'],
            ILogAble::CATEGORY['SERVICE_CATEGORY'],
            $this->enableAble->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->enableAble->getName()
        );
    }
}
