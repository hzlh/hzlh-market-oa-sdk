<?php
namespace  ProductMarket\LearnMarket\ServiceCategory\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\LearnMarket\ServiceCategory\View\ServiceCategoryViewTrait;

class ServiceCategoryView extends JsonView implements IView
{
    use ServiceCategoryViewTrait;
    
    public function display() : void
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getServiceCategory()
        );

        $this->encode($data);
    }
}
