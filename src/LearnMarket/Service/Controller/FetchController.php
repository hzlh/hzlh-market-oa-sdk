<?php
namespace  ProductMarket\LearnMarket\Service\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalRoleCheckTrait;
use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;

use ProductMarket\LearnMarket\Service\View\Json\ServiceView;
use ProductMarket\LearnMarket\Service\View\Json\ServiceListView;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\IModifyStatusAble;
use Sdk\Role\Model\IRoleAble;
use Sdk\LearnMarket\Service\Repository\ServiceRepository;

use Tag\Controller\TagControllerTrait;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalRoleCheckTrait, TagControllerTrait;

    const SCENE = array(
        'ALL' => 0,
        'PENDING' => 1,
        'APPROVE' => 2,
        'REJECT' => 3
    );

    const SEARCH = array(
        'TITLE' => 'title',
        'NUMBER' => 'number'
    );

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }

    protected function filterAction()
    {
        $search = $this->getRequest()->get('search', array());
        $searchType = $this->getRequest()->get('searchType', '');
        $scene = $this->getRequest()->get('scene', '');
        $scene = marmot_decode($scene);

        list($size,$page) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($searchType, $search, $scene);

        $serviceList = array();

        list($count, $serviceList) =
            $this->getRepository()
            ->scenario(ServiceRepository::OA_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
            
        $this->render(new ServiceListView($serviceList, $count));
        return true;
    }

    protected function implodeArray(array $array) : string
    {
        $idDecodeData = array();

        foreach ($array as $val) {
            $idDecodeData[] = marmot_decode($val);
        }

        return implode(',', $idDecodeData);
    }

    protected function filterFormatChange($searchType, $search, $scene)
    {
        $serviceCategory = $this->getRequest()->get('serviceCategory', array());

        $sort = ['-updateTime'];
        $filter = array();

        if (!empty($search[0])) {
            if ($searchType == self::SEARCH['TITLE']) {
                $filter['title'] = $search[0];
            }
            if ($searchType == self::SEARCH['NUMBER']) {
                $filter['number'] = $search[0];
            }
        }

        if ($scene != self::SCENE['ALL']) {
            $filter['status'] = IModifyStatusAble::STATUS['NORMAL'];
        }

        if ($scene == self::SCENE['PENDING']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['PENDING'];
        }
        if ($scene == self::SCENE['APPROVE']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        }
        if ($scene == self::SCENE['REJECT']) {
            $filter['applyStatus'] = IApplyAble::APPLY_STATUS['REJECT'];
        }

        if (!empty($serviceCategory)) {
            if (is_string($serviceCategory)) {
                $serviceCategory = array($serviceCategory);
            }

            $filter['serviceCategory'] = $this->implodeArray($serviceCategory);
        }

        return [$filter, $sort];
    }

    protected function fetchOneAction(int $id)
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $service = $this->getRepository()
        ->scenario(ServiceRepository::FETCH_ONE_MODEL_UN)
        ->fetchOne($id);
        
        if ($service instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $tag = $service->getTag();
        $tagList = $this->getTags($tag);

        $this->render(new ServiceView($service, $tagList));
        return true;
    }
}
