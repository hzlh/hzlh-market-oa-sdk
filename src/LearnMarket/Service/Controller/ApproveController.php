<?php
namespace  ProductMarket\LearnMarket\Service\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalRoleCheckTrait;
use Common\Controller\Traits\ApproveControllerTrait;
use Common\Controller\Interfaces\IApproveAbleController;

use ProductMarket\LearnMarket\Service\Command\Service\ApproveServiceCommand;
use ProductMarket\LearnMarket\Service\Command\Service\RejectServiceCommand;
use ProductMarket\LearnMarket\Service\CommandHandler\Service\ServiceCommandHandlerFactory;

class ApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait, GlobalRoleCheckTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function approveAction(int $id) : bool
    {
        return $this->getCommandBus()->send(new ApproveServiceCommand($id));
    }

    protected function rejectAction(int $id) : bool
    {
        $request = $this->getRequest();

        $rejectReason = $request->post('rejectReason', '');

        $command = new RejectServiceCommand(
            $rejectReason,
            $id
        );
            
        return $this->getCommandBus()->send($command);
    }
}
