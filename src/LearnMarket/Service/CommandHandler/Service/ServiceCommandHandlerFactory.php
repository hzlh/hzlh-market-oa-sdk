<?php
namespace  ProductMarket\LearnMarket\Service\CommandHandler\Service;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Framework\Classes\NullCommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;

class ServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'ProductMarket\LearnMarket\Service\Command\Service\RejectServiceCommand'=>
        'ProductMarket\LearnMarket\Service\CommandHandler\Service\RejectServiceCommandHandler',
        'ProductMarket\LearnMarket\Service\Command\Service\ApproveServiceCommand'=>
        'ProductMarket\LearnMarket\Service\CommandHandler\Service\ApproveServiceCommandHandler',
        'ProductMarket\LearnMarket\Service\Command\Service\AddServiceCommand'=>
            'ProductMarket\LearnMarket\Service\CommandHandler\Service\AddServiceCommandHandler',
        'ProductMarket\LearnMarket\Service\Command\Service\EditServiceCommand'=>
            'ProductMarket\LearnMarket\Service\CommandHandler\Service\EditServiceCommandHandler',
        'ProductMarket\LearnMarket\Service\Command\Service\OnShelfServiceCommand'=>
            'ProductMarket\LearnMarket\Service\CommandHandler\Service\OnShelfServiceCommandHandler',
        'ProductMarket\LearnMarket\Service\Command\Service\OffStockServiceCommand'=>
            'ProductMarket\LearnMarket\Service\CommandHandler\Service\OffStockServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : new NullCommandHandler();
    }
}
