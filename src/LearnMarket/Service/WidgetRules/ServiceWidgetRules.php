<?php

namespace ProductMarket\LearnMarket\Service\WidgetRules;

use Respect\Validation\Validator as V;

use Marmot\Core;

class ServiceWidgetRules
{
    const TITLE_MIN_LENGTH = 2;
    const TITLE_MAX_LENGTH = 50;

    public function title($title) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::TITLE_MIN_LENGTH,
            self::TITLE_MAX_LENGTH
        )->validate($title)) {
            Core::setLastError(TITLE_FORMAT_ERROR, array('pointer'=>'title'));
            return false;
        }

        return true;
    }

    const SERVICE_OBJECTS = array(
        'NULL' => 0,
        'MICRO_ENTERPRISE' => 1, //微型企业
        'SMALL_ENTERPRISES' => 2, //小型企业
        'MEDIUM_ENTERPRISE' => 3, //中型企业
        'LARGE_ENTERPRISES' => 4, //大型企业
        'ENTREPRENEURIAL_INDIVIDUAL' => 5, //创业个人
        'INDIVIDUAL_MERCHANTS' => 6, //个体商户
        'OTHER' => 7, //其他
    );

    public function serviceObjects($serviceObjects) : bool
    {
        foreach ($serviceObjects as $each) {
            if (!V::intVal()->validate($each) || !in_array($each, self::SERVICE_OBJECTS)) {
                Core::setLastError(SERVICE_OBJECT_NOT_EXIST);
                return false;
            }
        }
        return true;
    }

    public function contract($contract) : bool
    {
        if (!V::arrayType()->validate($contract)) {
            Core::setLastError(SERVICE_PRICE_FORMAT_ERROR);
            return false;
        }

        if (!isset($contract['identify']) || !$this->validateContractExtension($contract['identify'])) {
            Core::setLastError(SERVICE_CONTRACT_FORMAT_ERROR);
            return false;
        }

        return true;
    }

    private function validateContractExtension(string $contract) : bool
    {
        if (!V::extension('pdf')->validate($contract)) {
            return false;
        }

        return true;
    }

    const PRICE_NAME_MIN_LENGTH = 1;
    const PRICE_NAME_MAX_LENGTH = 15;
    const PRICE_VALUE_MIN_LENGTH = 0.01;
    const PRICE_VALUE_MAX_LENGTH = 50000;
    const PRICE_MAX_COUNT = 10;

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function servicePrice($price) : bool
    {
        $reg = '/^(?!.{12,}$)\d+(\.\d{1,2})?$/';

        if (!V::arrayType()->validate($price) || count($price) > self::PRICE_MAX_COUNT) {
            Core::setLastError(SERVICE_PRICE_FORMAT_ERROR);
            return false;
        }

        foreach ($price as $each) {
            if (!isset($each['name']) || !isset($each['value'])) {
                Core::setLastError(SERVICE_PRICE_FORMAT_ERROR);
                return false;
            }

            if (!V::charset('UTF-8')->stringType()->length(
                self::PRICE_NAME_MIN_LENGTH,
                self::PRICE_NAME_MAX_LENGTH
            )->validate($each['name'])) {
                Core::setLastError(SERVICE_PRICE_FORMAT_ERROR);
                return false;
            }

            if (!preg_match($reg, $each['value'])) {
                Core::setLastError(SERVICE_PRICE_FORMAT_ERROR);
                return false;
            }

            if (!V::charset('UTF-8')->between(
                self::PRICE_VALUE_MIN_LENGTH,
                self::PRICE_VALUE_MAX_LENGTH
            )->validate($each['value'])) {
                Core::setLastError(SERVICE_PRICE_FORMAT_ERROR);
                return false;
            }
        }

        return true;
    }
}