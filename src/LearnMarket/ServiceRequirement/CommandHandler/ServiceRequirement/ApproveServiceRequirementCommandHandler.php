<?php
namespace  ProductMarket\LearnMarket\ServiceRequirement\CommandHandler\ServiceRequirement;

use Marmot\Core;

use Sdk\Common\Model\IApplyAble;
use Sdk\Common\CommandHandler\ApproveCommandHandler;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class ApproveServiceRequirementCommandHandler extends ApproveCommandHandler implements ILogAble
{
    use ServiceRequirementCommandHandlerTrait, LogDriverCommandHandlerTrait;
    
    protected function fetchIApplyObject($id) : IApplyAble
    {
        return $this->fetchServiceRequirement($id);
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_APPROVE'],
            ILogAble::CATEGORY['SERVICE_REQUIREMENT'],
            $this->approveAble->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->approveAble->getNumber()
        );
    }
}
