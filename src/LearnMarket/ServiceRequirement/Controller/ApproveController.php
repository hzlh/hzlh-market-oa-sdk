<?php
namespace  ProductMarket\LearnMarket\ServiceRequirement\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\ApproveControllerTrait;
use Common\Controller\Interfaces\IApproveAbleController;

use ProductMarket\LearnMarket\ServiceRequirement\Command\ServiceRequirement\ApproveServiceRequirementCommand;
use ProductMarket\LearnMarket\ServiceRequirement\Command\ServiceRequirement\RejectServiceRequirementCommand;
use ProductMarket\LearnMarket\ServiceRequirement\CommandHandler\ServiceRequirement\ServiceRequirementCommandHandlerFactory;

use Common\Controller\Traits\GlobalRoleCheckTrait;

class ApproveController extends Controller implements IApproveAbleController
{
    use WebTrait, ApproveControllerTrait, GlobalRoleCheckTrait;

    private $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ServiceRequirementCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    protected function approveAction(int $id) : bool
    {
//        if (!$this->globalRoleCheck(
//            IRoleAble::CATEGORY['SERVICE_REQUIREMENT'],
//            IRoleAble::OPERATION['OPERATION_REJECT']
//        )) {
//            return false;
//        }

        return $this->getCommandBus()->send(new ApproveServiceRequirementCommand($id));
    }

    protected function rejectAction(int $id) : bool
    {
//        if (!$this->globalRoleCheck(
//            IRoleAble::CATEGORY['SERVICE_REQUIREMENT'],
//            IRoleAble::OPERATION['OPERATION_REJECT']
//        )) {
//            return false;
//        }
      
        $request = $this->getRequest();
        
        $rejectReason = $request->post('rejectReason', '');

        $command = new RejectServiceRequirementCommand(
            $rejectReason,
            $id
        );
     
        return $this->getCommandBus()->send($command);
    }
}
