<?php
namespace  ProductMarket\LearnMarket\ServiceRequirement\View;

use Sdk\LearnMarket\ServiceRequirement\Model\ServiceRequirement;
use ProductMarket\LearnMarket\ServiceRequirement\Translator\ServiceRequirementTranslator;

trait ServiceRequirementViewTrait
{
    private $serviceRequirement;

    private $translator;

    public function __construct(ServiceRequirement $serviceRequirement)
    {
        $this->serviceRequirement = $serviceRequirement;
        $this->translator = new ServiceRequirementTranslator();
        parent::__construct();
    }

    protected function getServiceRequirement() : ServiceRequirement
    {
        return $this->serviceRequirement;
    }

    protected function getTranslator() : ServiceRequirementTranslator
    {
        return $this->translator;
    }
}
