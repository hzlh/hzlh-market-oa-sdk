<?php
namespace  ProductMarket\LearnMarket\ServiceRequirement\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\LearnMarket\ServiceRequirement\Translator\ServiceRequirementTranslator;

class ServiceRequirementListView extends JsonView implements IView
{
    private $serviceRequirementList;

    private $count;

    private $translator;

    public function __construct(
        array $serviceRequirementList,
        int $count
    ) {
        $this->serviceRequirementList = $serviceRequirementList;
        $this->count = $count;
        $this->translator = new ServiceRequirementTranslator();
        parent::__construct();
    }

    protected function getServiceRequirementList() : array
    {
        return $this->serviceRequirementList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : ServiceRequirementTranslator
    {
        return $this->translator;
    }

    public function display() : void
    {
        $data = array();
        $translator = $this->getTranslator();

        foreach ($this->getServiceRequirementList() as $serviceRequirement) {
            $data[] = $translator->objectToArray(
                $serviceRequirement,
                array(
                    'id',
                    'title',
                    'number',
                    'detail',
                    'serviceCategory'=>[],
                    'contactName',
                    'contactPhone',
                    'applyStatus',
                    'status',
                    'updateTime'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
