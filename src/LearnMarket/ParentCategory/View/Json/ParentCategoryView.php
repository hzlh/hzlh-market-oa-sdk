<?php
namespace  ProductMarket\LearnMarket\ParentCategory\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\LearnMarket\ParentCategory\View\ParentCategoryViewTrait;

class ParentCategoryView extends JsonView implements IView
{
    use ParentCategoryViewTrait;
    
    public function display() : void
    {
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getParentCategory()
        );

        $this->encode($data);
    }
}
