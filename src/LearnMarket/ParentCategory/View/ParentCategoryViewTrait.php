<?php
namespace  ProductMarket\LearnMarket\ParentCategory\View;

use Sdk\LearnMarket\TalentMarket\ServiceCategory\Model\ParentCategory;
use ProductMarket\LearnMarket\ParentCategory\Translator\ParentCategoryTranslator;

trait ParentCategoryViewTrait
{
    private $parentCategory;

    private $translator;

    public function __construct(ParentCategory $parentCategory)
    {
        $this->parentCategory = $parentCategory;
        $this->translator = new ParentCategoryTranslator();
        parent::__construct();
    }

    protected function getParentCategory() : ParentCategory
    {
        return $this->parentCategory;
    }

    protected function getTranslator() : ParentCategoryTranslator
    {
        return $this->translator;
    }
}
