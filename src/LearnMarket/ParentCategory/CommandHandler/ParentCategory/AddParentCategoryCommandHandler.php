<?php
namespace  ProductMarket\LearnMarket\ParentCategory\CommandHandler\ParentCategory;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\LearnMarket\ServiceCategory\Model\ParentCategory;

use LearnMarket\ParentCategory\Command\ParentCategory\AddParentCategoryCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class AddParentCategoryCommandHandler implements ICommandHandler, ILogAble
{
    use LogDriverCommandHandlerTrait;

    private $parentCategory;

    public function __construct()
    {
        $this->parentCategory = new ParentCategory();
    }

    public function __destruct()
    {
        unset($this->parentCategory);
    }

    protected function getParentCategory() : ParentCategory
    {
        return $this->parentCategory;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddParentCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $parentCategory = $this->getParentCategory();
        $parentCategory->setName($command->name);
        
        if ($parentCategory->add()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_ADD'],
            ILogAble::CATEGORY['PARENT_CATEGORY'],
            $this->getParentCategory()->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->getParentCategory()->getName()
        );
    }
}
