<?php
namespace  ProductMarket\LearnMarket\ParentCategory\CommandHandler\ParentCategory;

use Marmot\Core;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use LearnMarket\ParentCategory\Command\ParentCategory\EditParentCategoryCommand;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogAble;
use Sdk\Common\CommandHandler\LogDriverCommandHandlerTrait;

class EditParentCategoryCommandHandler implements ICommandHandler, ILogAble
{
    use ParentCategoryCommandHandlerTrait, LogDriverCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditParentCategoryCommand)) {
            throw new \InvalidArgumentException;
        }

        $this->parentCategory = $this->fetchParentCategory($command->id);
        $this->parentCategory->setName($command->name);

        if ($this->parentCategory->edit()) {
            $this->logDriverInfo($this);
            return true;
        }

        $this->logDriverError($this);
        return false;
    }

    public function getLog() : Log
    {
        return new Log(
            ILogAble::OPERATION['OPERATION_EDIT'],
            ILogAble::CATEGORY['PARENT_CATEGORY'],
            $this->parentCategory->getId(),
            Log::TYPE['CREW'],
            Core::$container->get('crew'),
            $this->parentCategory->getName()
        );
    }
}
