<?php
namespace ProductMarket\Order\ServiceOrder\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use Sdk\ProductMarket\Order\ServiceOrder\Model\NullServiceOrder;
use Sdk\Payment\Model\Payment;

use Enterprise\Translator\EnterpriseTranslator;
use ProductMarket\MemberAccount\Translator\MemberAccountTranslator;
use MemberCoupon\Translator\MemberCouponTranslator;

use ProductMarket\Order\CommonOrder\Translator\OrderAddressTranslator;
use ProductMarket\Order\CommonOrder\Translator\OrderCommodityTranslator;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class ServiceOrderTranslator implements ITranslator
{
    const EAXCEL_STATUS_CN = array(
        0 => '等待买家付款',
        2 =>  '等待卖家接单',
        4 =>  '进行中',
        6 => '等待买家确认',
        8 => '已完成',
        9 => '已完成',
        10 => '已完成',
        -2 => '已关闭',
        -3 => '已关闭',
        -4 => '已关闭',
        -6 => '支付失败',
        -8 => '退款中',
        -10 =>'退款完成',
    );

    protected function getMemberAccountTranslator() : MemberAccountTranslator
    {
        return new MemberAccountTranslator();
    }

    protected function getOrderAddressTranslator() : OrderAddressTranslator
    {
        return new OrderAddressTranslator();
    }

    protected function getOrderCommodityTranslator() : OrderCommodityTranslator
    {
        return new OrderCommodityTranslator();
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return new EnterpriseTranslator();
    }

    protected function getMemberCouponTranslator() : MemberCouponTranslator
    {
        return new MemberCouponTranslator();
    }
    
    protected function getOrderUpdateAmountRecordTranslator() : OrderUpdateAmountRecordTranslator
    {
        return new OrderUpdateAmountRecordTranslator();
    }

    public function arrayToObject(array $expression, $serviceOrder = null)
    {
        unset($serviceOrder);
        unset($expression);
        return NullServiceOrder::getInstance();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($serviceOrder, array $keys = array())
    {
        $expression = array();

        if (!$serviceOrder instanceof ServiceOrder) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'orderno',
                'totalPrice',
                'paidAmount',
                'collectedAmount',
                'realAmount',
                'commission',
                'subsidy',
                'platformPreferentialAmount',
                'businessPreferentialAmount',
                'paymentType',
                'transactionNumber',
                'transactionInfo',
                'paymentTime',
                'status',
                'remark',
                'timeRecord',
                'failureReason',
                'cancelReason',
                'buyerOrderStatus',
                'sellerOrderStatus',
                'createTime',
                'updateTime',
                'statusTime',
                'sellerEnterprise'=>[],
                'buyerMemberAccount' => [],
                'orderUpdateAmountRecord'=>[],
                'orderAddress' => [],
                'memberCoupons' => [],
                'orderCommodities' => []
            );
        }

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($serviceOrder->getId());
        }
      
        if (in_array('orderno', $keys)) {
            $expression['orderno'] = $serviceOrder->getOrderno();
            $expression['oldOrderno'] = date('Ymd', $serviceOrder->getCreateTime()).$serviceOrder->getId();
        }

        if (in_array('totalPrice', $keys)) {
            $expression['totalPrice'] = $serviceOrder->getTotalPrice();
        }

        if (in_array('paidAmount', $keys)) {
            $expression['paidAmount'] = $serviceOrder->getPaidAmount();
        }

        if (in_array('collectedAmount', $keys)) {
            $expression['collectedAmount'] = $serviceOrder->getCollectedAmount();
        }

        if (in_array('realAmount', $keys)) {
            $expression['realAmount'] = $serviceOrder->getRealAmount();
        }

        if (in_array('commission', $keys)) {
            $expression['commission'] = $serviceOrder->getCommission();
        }

        if (in_array('subsidy', $keys)) {
            $expression['subsidy'] = $serviceOrder->getSubsidy();
        }

        if (in_array('platformPreferentialAmount', $keys)) {
            $expression['platformPreferentialAmount'] = round($serviceOrder->getPlatformPreferentialAmount(), 2);
        }

        if (in_array('businessPreferentialAmount', $keys)) {
            $expression['businessPreferentialAmount'] = round($serviceOrder->getBusinessPreferentialAmount(), 2);
        }

        if (in_array('paymentType', $keys)) {
            $expression['paymentType'] = $serviceOrder->getPayment()->getType();
            $expression['paymentTypeFormat'] = Payment::PAYMENT_TYPE_CN[$expression['paymentType']];
        }

        if (in_array('transactionNumber', $keys)) {
            $expression['transactionNumber'] = $serviceOrder->getPayment()->getTransactionNumber();
        }

        if (in_array('transactionInfo', $keys)) {
            $expression['transactionInfo'] = $serviceOrder->getPayment()->getTransactionInfo();
        }

        if (in_array('paymentTime', $keys)) {
            $expression['paymentTime'] = $serviceOrder->getPayment()->getTime();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $serviceOrder->getStatus();
            $expression['statusFormat'] = ServiceOrder::OA_STATUS_CN[$serviceOrder->getStatus()];
            $expression['statusTypeFormat'] = self::EAXCEL_STATUS_CN[$serviceOrder->getStatus()];
        }

        if (in_array('remark', $keys)) {
            $expression['remark'] = $serviceOrder->getRemark();
        }

        if (in_array('cancelReason', $keys)) {
            $expression['cancelReason'] = $serviceOrder->getCancelReason();
        }

        if (in_array('timeRecord', $keys)) {
            $expression['timeRecord'] = $serviceOrder->getTimeRecord();
        }

        // if (in_array('failureReason', $keys)) {
        //     $expression['failureReason'] = $serviceOrder->getFailureReason();
        // }

        if (in_array('buyerOrderStatus', $keys)) {
            $expression['buyerOrderStatus'] = $serviceOrder->getBuyerOrderStatus();
        }

        if (in_array('sellerOrderStatus', $keys)) {
            $expression['sellerOrderStatus'] = $serviceOrder->getSellerOrderStatus();
        }

        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $serviceOrder->getCreateTime();
            $expression['createTimeFormat'] = date('Y-m-d H:i:s', $serviceOrder->getCreateTime());
        }

        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $serviceOrder->getUpdateTime();
            $expression['updateTimeFormat'] = date('Y-m-d H:i:s', $serviceOrder->getUpdateTime());
        }

        if (in_array('statusTime', $keys)) {
            $expression['statusTime'] = $serviceOrder->getStatusTime();
        }

        if (isset($keys['buyerMemberAccount'])) {
            $expression['buyerMemberAccount'] = $this->getMemberAccountTranslator()->objectToArray(
                $serviceOrder->getBuyerMemberAccount(),
                $keys['buyerMemberAccount']
            );
        }

        if (isset($keys['orderUpdateAmountRecord'])) {
            $expression['orderUpdateAmountRecord'] = $this->getOrderUpdateAmountRecordTranslator()->objectToArray(
                $serviceOrder->getOrderUpdateAmountRecord(),
                $keys['orderUpdateAmountRecord']
            );
        }

        if (isset($keys['sellerEnterprise'])) {
            $expression['sellerEnterprise'] = $this->getEnterpriseTranslator()->objectToArray(
                $serviceOrder->getSellerEnterprise(),
                $keys['sellerEnterprise']
            );
        }

        if (isset($keys['orderAddress'])) {
            $expression['orderAddress'] = $this->getOrderAddressTranslator()->objectToArray(
                $serviceOrder->getOrderAddress(),
                $keys['orderAddress']
            );
        }

        if (isset($keys['memberCoupons'])) {
            $memberCoupons = $serviceOrder->getMemberCoupons();
            foreach ($memberCoupons as $memberCoupon) {
                $expression['memberCoupons'][] = $this->getMemberCouponTranslator()->objectToArray(
                    $memberCoupon,
                    $keys['memberCoupons']
                );
            }
        }

        if (isset($keys['orderCommodities'])) {
            $orderCommodities = $serviceOrder->getOrderCommodities();
            foreach ($orderCommodities as $orderCommodity) {
                $expression['orderCommodities'][] = $this->getOrderCommodityTranslator()->objectToArray(
                    $orderCommodity,
                    $keys['orderCommodities']
                );
            }
        }

        return $expression;
    }
}
