<?php
namespace ProductMarket\Order\ServiceOrder\Translator;

use Marmot\Interfaces\ITranslator;
use Sdk\Statistical\Model\NullStatistical;
use Sdk\Statistical\Model\Statistical;

class StaticsFinishOrderByEnterpriseTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {
        unset($statistical);
        unset($expression);
        return NullStatistical::getInstance();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($statistical, array $keys = array())
    {
        if (!$statistical instanceof Statistical) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'result',
            );
        }

        $expression = array();

        if (in_array('result', $keys)) {
            $expression = $statistical->getResult();

            $expression['list'] = $expression['list'] ?? array();
            $expression['count'] =  $expression['count'] ?? 0;
            $list = array();
            foreach ($expression['list'] as $index => $value) {
                $list[$index]['sellerEnterpriseId'] = marmot_encode($value['seller_enterprise_id']);
                $list[$index]['sellerEnterpriseName'] = isset($value['seller_enterprise_name'])
                    ? $value['seller_enterprise_name']
                    : '';
                $list[$index]['finishOrderTotal'] = $value['finish_order_total'];
                $list[$index]['sumTotalPrice'] = $value['sum_total_price'];
                $list[$index]['sumTotalPriceFormatTenThousand'] = bcdiv($value['sum_total_price'], 10000, 2);
                $list[$index]['sumPaidAmount'] = $value['sum_paid_amount'];
                $list[$index]['sumPaidAmountFormatTenThousand'] = bcdiv($value['sum_paid_amount'], 10000, 2);
                $list[$index]['sumRealAmount'] = $value['sum_real_amount'];
                $list[$index]['sumRealAmountFormatTenThousand'] = bcdiv($value['sum_real_amount'], 10000, 2);
            }
            $expression['list'] = $list;
        }

        return $expression;
    }
}
