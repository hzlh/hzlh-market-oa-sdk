<?php
namespace ProductMarket\Order\ServiceOrder\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Order\CommonOrder\Model\OrderUpdateAmountRecord;
use Sdk\ProductMarket\Order\CommonOrder\Model\NullOrderUpdateAmountRecord;

/**
 * 屏蔽类中所有PMD警告
 * @SuppressWarnings(PHPMD)
 */
class OrderUpdateAmountRecordTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $orderUpdateAmountRecord = null)
    {
        unset($orderUpdateAmountRecord);
        unset($expression);
        return NullOrderUpdateAmountRecord::getInstance();
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderUpdateAmountRecord, array $keys = array())
    {
        $expression = array();

        if (!$orderUpdateAmountRecord instanceof OrderUpdateAmountRecord) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'amount',
                'remark',
                'createTime'
            );
        }

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($orderUpdateAmountRecord->getId());
        }
        if (in_array('amount', $keys)) {
            $expression['amount'] = $orderUpdateAmountRecord->getAmount();
            $expression['amountFormat'] = number_format($expression['amount'], 2, '.', '');
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = $orderUpdateAmountRecord->getRemark();
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $orderUpdateAmountRecord->getCreateTime();
        }

        return $expression;
    }
}
