<?php
namespace ProductMarket\Order\ServiceOrder\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\Order\ServiceOrder\View\ServiceOrderListViewTrait;

class ServiceOrderListView extends JsonView implements IView
{
    use ServiceOrderListViewTrait;

    public function display() : void
    {
        $data = $this->getOrderList();
        
        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data
        );

        $this->encode($dataList);
    }
}
