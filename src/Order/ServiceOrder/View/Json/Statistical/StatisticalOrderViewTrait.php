<?php
namespace ProductMarket\Order\ServiceOrder\View\Json\Statistical;

use Marmot\Interfaces\ITranslator;

use ProductMarket\Order\ServiceOrder\Translator\StaticsFinishOrderByEnterpriseTranslator;

use Sdk\Statistical\Model\Statistical;

trait StatisticalOrderViewTrait
{
    private $staticsResult;
    private $staticsTranslator;

    public function __construct(
        Statistical $staticsResult,
        int $page = 1,
        int $limit = 0
    ) {
        parent::__construct();
        
        $this->staticsResult = $staticsResult;
        $this->staticsTranslator = new StaticsFinishOrderByEnterpriseTranslator();
        $this->page = $page;
        $this->limit = $limit;
    }

    protected function getStaticsResult() : Statistical
    {
        return $this->staticsResult;
    }

    protected function getStaticsTranslator() : ITranslator
    {
        return $this->staticsTranslator;
    }

    protected function getPage() : int
    {
        return $this->page;
    }

    protected function getLimit() : int
    {
        return $this->limit;
    }

    protected function getAllData() : array
    {
        $result = $this->getStaticsTranslator()->objectToArray($this->getStaticsResult());

        $page = $this->getPage();
        $page = $page - 1 >= 0 ? $page-1 : 0;
        $limit = $this->getLimit();

        foreach ($result['list'] as $key => $value) {
            $result['list'][$key]['number'] = $limit == 0 || $page == 0 ? $key+1 : 1 + $key + ($limit*$page);
            unset($value);
        }

        return $result;
    }
}
