<?php
namespace ProductMarket\Order\ServiceOrder\View\Json\Statistical;

use Marmot\Interfaces\IView;

use Common\View\ExcelView;

use ProductMarket\Order\ServiceOrder\View\Json\Statistical\StatisticalOrderViewTrait;

class StatisticalFinishOrderListExcelView extends ExcelView implements IView
{
    use StatisticalOrderViewTrait;

    const TITILS = array(
        'number' => '排行',
        'sellerEnterpriseName'=> '企业名称',
        'finishOrderTotal' => '成交订单',
        'sumTotalPrice' => '订单原价',
        'sumPaidAmount' => '买家实付',
        'sumRealAmount' => '商家实收',
    );

    public function display() : void
    {
        $data = $this->getAllData()['list'];

        $titles = self::TITILS;
        $title = '汇众联合运营排行表';

        $this->export($titles, $data, $title);
    }
}
