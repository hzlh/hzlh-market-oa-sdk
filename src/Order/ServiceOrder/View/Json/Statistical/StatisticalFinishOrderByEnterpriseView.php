<?php
namespace ProductMarket\Order\ServiceOrder\View\Json\Statistical;

use Marmot\Framework\View\Json\JsonView;

use Marmot\Interfaces\IView;

use ProductMarket\Order\ServiceOrder\View\Json\Statistical\StatisticalOrderViewTrait;

class StatisticalFinishOrderByEnterpriseView extends JsonView implements IView
{
    use StatisticalOrderViewTrait;

    public function display() : void
    {
        $data = $this->getAllData();

        $dataList = array(
            'total' => $data['count'],
            'list' => $data['list']
        );

        $this->encode($dataList);
    }
}
