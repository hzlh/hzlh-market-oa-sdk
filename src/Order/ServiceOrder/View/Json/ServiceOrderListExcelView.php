<?php
namespace ProductMarket\Order\ServiceOrder\View\Json;

use Marmot\Interfaces\IView;
use Common\View\ExcelView;

use ProductMarket\Order\ServiceOrder\View\ServiceOrderListViewTrait;

class ServiceOrderListExcelView extends ExcelView implements IView
{
    use ServiceOrderListViewTrait;

    const TITILS = array(
        'orderno' => '订单号',
        'goods'=> '商品',
        'price' => '单价（元）',
        'number' => '数量',
        'amount' => '订单原价（元）',
        'modifyPrice' => '调价金额（元）',
        'dealPrice' => '成交价格（元）',
        'paidAmount' => '买家实付（元）',
        'collectedAmount' => '卖家实收（元）',
        'statusTypeFormat' => '订单状态',
        'buyerInfo' => '买家信息',
        'sellerEnterprise' => '卖家信息',
        'createTimeFormat' => '创建时间',
        'updateTimeFormat' => '更新时间',
    );

    protected function getExcelList() : array
    {
        $resultList = [];

        $orderList = $this->getOrderList();
        foreach ($orderList as $key => $value) {
            $title = $value['orderCommodities'][0]['commodity']['title'];
            $priceLabel = $value['orderCommodities'][0]['commodity']['price'][0]['name'];
            $value['goods'] = $title.'-'.'规格:'.$priceLabel;

            $value['price'] = $value['orderCommodities'][0]['commodity']['price'][0]['value'];
            $value['number'] = $value['orderCommodities'][0]['number'];
            $value['amount'] = $value['orderCommodities'][0]['commodity']['price'][0]['value'];

            $name = $value['orderAddress']['deliveryAddress']['realName'];
            $cellphone = $value['orderAddress']['deliveryAddress']['cellphone'];
            $area = $value['orderAddress']['deliveryAddress']['area'];
            $value['buyerInfo'] = $name.'-'.$cellphone.'-'.$area;

            $value['sellerEnterprise'] = $value['sellerEnterprise']['name'];

            $resultList[$key] = $value;
        }
        return $resultList;
    }
    
    public function display() : void
    {
        $data = $this->getExcelList();
       
        $titles = self::TITILS;
        $title = '汇众联合企业运营数据表';

        $this->export($titles, $data, $title);
    }
}
