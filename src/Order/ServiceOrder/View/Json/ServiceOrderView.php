<?php
namespace ProductMarket\Order\ServiceOrder\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use ProductMarket\Order\ServiceOrder\View\ServiceOrderViewTrait;

class ServiceOrderView extends JsonView implements IView
{
    use ServiceOrderViewTrait;
    
    public function display() : void
    {
        $translator = $this->getTranslator();

        $enterpriseTranslator = $this->getEnterpriseTranslator();

        $data = $translator->objectToArray(
            $this->getServiceOrder()
        );

        $buyerEnterprise = array();
        
        $buyerEnterprise = $enterpriseTranslator->objectToArray(
            $this->getBuyerEnterprise()
        );

        $data['buyerEnterprise'] = $buyerEnterprise;
        // 成交价格
        $data['dealPrice'] = empty($data['orderUpdateAmountRecord']['amount']) ?
                                $data['totalPrice'] :
                                $data['orderUpdateAmountRecord']['amount'];
        // 调价金额
        $data['modifyPrice'] = $data['dealPrice']-$data['totalPrice'];

        $this->encode($data);
    }
}
