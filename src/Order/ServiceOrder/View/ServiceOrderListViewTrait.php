<?php
namespace ProductMarket\Order\ServiceOrder\View;

use ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator;

trait ServiceOrderListViewTrait
{
    private $serviceOrderList;

    private $count;

    private $translator;

    public function __construct(
        array $serviceOrderList,
        int $count
    ) {
        parent::__construct();
        
        $this->serviceOrderList = $serviceOrderList;
        $this->count = $count;
        $this->translator = new ServiceOrderTranslator();
    }

    protected function getServiceOrderList() : array
    {
        return $this->serviceOrderList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : ServiceOrderTranslator
    {
        return $this->translator;
    }

    protected function getOrderList():array
    {
        $data = array();

        $translator = $this->getTranslator();

        foreach ($this->getServiceOrderList() as $serviceOrder) {
            $data[] = $translator->objectToArray(
                $serviceOrder,
                array(
                    'id',
                    'orderno',
                    'orderCommodities'=>['number', 'skuIndex', 'commodity'=>['title','price']],
                    'totalPrice',
                    'paidAmount',
                    'platformPreferentialAmount',
                    'businessPreferentialAmount',
                    'collectedAmount',
                    'status',
                    'orderAddress'=>['deliveryAddress'=>['realName','cellphone','area','address']],
                    'sellerEnterprise'=>['name'],
                    'orderUpdateAmountRecord'=>[],
                    'createTime',
                    'updateTime'
                )
            );
        }

        if (!empty($data)) {
            foreach ($data as $key => $item) {
                // 成交价
                $data[$key]['dealPrice'] = empty($item['orderUpdateAmountRecord']['amount']) ?
                                            $item['totalPrice'] :
                                            $item['orderUpdateAmountRecord']['amount'];
                // 调整价
                $data[$key]['modifyPrice'] = $data[$key]['dealPrice']-$item['totalPrice'];
            }
        }
    
        return $data;
    }
}
