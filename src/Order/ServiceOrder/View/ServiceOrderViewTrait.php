<?php
namespace ProductMarket\Order\ServiceOrder\View;

use Sdk\ProductMarket\Order\ServiceOrder\Model\ServiceOrder;
use ProductMarket\Order\ServiceOrder\Translator\ServiceOrderTranslator;
use Enterprise\Translator\EnterpriseTranslator;
use Sdk\Enterprise\Model\Enterprise;

trait ServiceOrderViewTrait
{
    private $serviceOrder;

    private $buyerEnterprise;

    private $translator;

    private $enterpriseTranslator;

    public function __construct(ServiceOrder $serviceOrder, Enterprise $buyerEnterprise)
    {
        $this->serviceOrder = $serviceOrder;
        $this->buyerEnterprise = $buyerEnterprise;
        $this->translator = new ServiceOrderTranslator();
        $this->enterpriseTranslator = new EnterpriseTranslator();
        parent::__construct();
    }

    protected function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    protected function getBuyerEnterprise() : Enterprise
    {
        return $this->buyerEnterprise;
    }

    protected function getTranslator() : ServiceOrderTranslator
    {
        return $this->translator;
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return $this->enterpriseTranslator;
    }
}
