<?php
namespace ProductMarket\Order\ServiceOrder\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\GlobalCheckTrait;
use Common\Controller\Traits\GlobalRoleCheckTrait;

use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;

use Statistical\Controller\StatisticalControllerTrait;

use ProductMarket\Order\ServiceOrder\View\Json\Statistical\StatisticalFinishOrderByEnterpriseView;
use ProductMarket\Order\ServiceOrder\View\Json\Statistical\StatisticalFinishOrderListExcelView;

class StatisticalController extends Controller
{
    use WebTrait,StatisticalControllerTrait, GlobalCheckTrait, GlobalRoleCheckTrait;

    const STATICS_TYPE = array(
        'FINISH_ORDER' => "staticsFinishOrderByEnterprise",
    );

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceOrderRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    public function staticsOrder() : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['ENTERPRISE_SORT_BY_ORDER'],
        //     IRoleAble::OPERATION['OPERATION_SHOW']
        // )
        // ) {
        //     $this->displayError();
        //     return false;
        // }

        $filter = $this->filterFormatChange();

        $statics = $this->analyse(self::STATICS_TYPE['FINISH_ORDER'], $filter);

        // 计算排名需要page
        $this->render(new StatisticalFinishOrderByEnterpriseView($statics, $filter['number'], $filter['size']));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        $enterpriseId = $request->get('enterpriseId', '');
        $enterpriseName = $request->get('enterpriseName', '');
        $size = $request->get('limit', 0);
        $page = $request->get('page', 0);
        $startTime = $request->get('startTime', 0);
        $endTime = $request->get('endTime', 0);

        if (!empty($enterpriseId)) {
            $filter['enterpriseId'] = marmot_decode($enterpriseId);
        }

        if (!empty($enterpriseName)) {
            $enterpriseIds = $this->getEnterpriseIds($enterpriseName);

            if (!empty($enterpriseIds)) {
                $filter['enterpriseIds'] = implode(',', $enterpriseIds);
            }
        }

        if ($startTime > 0) {
            $filter['startTime'] = $startTime;
        }
        if ($endTime > 0) {
            $filter['endTime'] = $endTime;
        }

        $filter['number'] = $page;
        $filter['size'] = $size;

        return $filter;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return new EnterpriseRepository();
    }

    protected function getEnterpriseIds(string $name = '') : array
    {
        if (empty($name)) {
            return array();
        }

        list($count, $enterpriseList) = $this->getEnterpriseRepository()
            ->scenario(EnterpriseRepository::OA_LIST_MODEL_UN)
            ->search(['name' => $name], ['-updateTime']);

        unset($count);

        $enterpriseIds = array();
        foreach ($enterpriseList as $item) {
            $enterpriseIds[] = $item->getId();
        }

        return $enterpriseIds;
    }

    protected function getStaticsList(array $filter = array())
    {
        $statics = $this->analyse(self::STATICS_TYPE['FINISH_ORDER'], $filter);

        return $statics;
    }

    //统计企业完成订单导出数据
    public function staticsSheetExcel() : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['ENTERPRISE_SORT_BY_ORDER'],
        //     IRoleAble::OPERATION['OPERATION_DOWNLOAD']
        // )
        // ) {
        //     $this->displayError();
        //     return false;
        // }
        
        $filter = $this->filterFormatChange();
        $filter['number'] = DEFAULT_PAGE;
        $filter['size'] = DEFAULT_PAGE;

        $statics = $this->getStaticsList($filter);
       
        $this->render(new StatisticalFinishOrderListExcelView($statics));
        return true;
    }
}
