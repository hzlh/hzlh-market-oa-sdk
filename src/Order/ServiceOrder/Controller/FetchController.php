<?php
namespace ProductMarket\Order\ServiceOrder\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Common\Controller\Traits\FetchControllerTrait;
use Common\Controller\Interfaces\IFetchAbleController;
use Common\Controller\Traits\GlobalRoleCheckTrait;

use ProductMarket\Order\ServiceOrder\View\Json\ServiceOrderListView;
use ProductMarket\Order\ServiceOrder\View\Json\ServiceOrderView;
use ProductMarket\Order\ServiceOrder\View\Json\ServiceOrderListExcelView;

use Sdk\ProductMarket\Order\ServiceOrder\Repository\ServiceOrderRepository;
use Sdk\ProductMarket\Order\CommonOrder\Model\Order;
use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Role\Model\IRoleAble;

class FetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, GlobalRoleCheckTrait;

    const SCENE = array(
        "ALL" =>1, //全部
        "WAITING_BUYER_PAY" =>2, //等待买家付款
        "WAITING_SELLER_RECEIVE" =>3, //等待卖家接单
        "IN_PROGRESS" =>4, //进行中
        "WAITING_BUYER_CONFIRM" =>5, //等待买家确认
        "COMPLETED" =>6, //已完成
        "CLOSED" =>7, //已关闭
        "REFUNDING" =>8, //退款中
        "REFUND" =>9, //退款完成
    );

    const STATUS = array(
        "COMPLETED" =>'8,9,10',
        "CLOSED" => '-2,-3,-4',
        "REFUNDING" => '-8',
        "REFUND" => '-10'
    );

    const MONTH_LENGTH = 30;

    private $repository;

    private $enterpriseRepository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ServiceOrderRepository();
        $this->enterpriseRepository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->enterpriseRepository);
    }

    protected function getRepository() : ServiceOrderRepository
    {
        return $this->repository;
    }

    protected function getEnterpriseRepository() : EnterpriseRepository
    {
        return $this->enterpriseRepository;
    }

    protected function filterAction() : bool
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['SERVICE_ORDER'],
        //     IRoleAble::OPERATION['OPERATION_SHOW']
        // )
        // ) {
        //     return false;
        // }

        list($count, $serviceOrderList) = $this->getOrderList();

        $this->render(new ServiceOrderListView($serviceOrderList, $count));
        return true;
    }

    protected function getOrderList()
    {
        list($size, $page) = $this->getPageAndSize();

        list($filter, $sort) = $this->filterFormatChange();
        
        list($count, $serviceOrderList) = $this->getRepository()
            ->scenario(ServiceOrderRepository::OA_LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
        
        return [$count, $serviceOrderList];
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function filterFormatChange()
    {
        $scene = $this->getRequest()->get('scene', '');
        $scene = marmot_decode($scene);
        $orderno = $this->getRequest()->get('orderno', '');
        $oldOrderno = $this->getRequest()->get('oldOrderno', '');
        $name = $this->getRequest()->get('name', '');
        $sellerName = $this->getRequest()->get('sellerName', '');
        $buyerCellphone = $this->getRequest()->get('buyerCellphone', '');
        $startTime = $this->getRequest()->get('startTime', '');
        $endTime = $this->getRequest()->get('endTime', '');
        $requestSort = $this->getRequest()->get('sort', '');

        $sort = ['-createTime'];
        if (!empty($requestSort)) {
            $sort = [$requestSort];
        }
        
        $filter = array();

        if ($scene == self::SCENE['WAITING_BUYER_PAY']) {
            $filter['status'] = Order::STATUS['PENDING'];
        }
        if ($scene == self::SCENE['WAITING_SELLER_RECEIVE']) {
            $filter['status'] = Order::STATUS['PAID'];
        }
        if ($scene == self::SCENE['IN_PROGRESS']) {
            $filter['status'] = Order::STATUS['PERFORMANCE_BEGIN'];
        }
        if ($scene == self::SCENE['WAITING_BUYER_CONFIRM']) {
            $filter['status'] = Order::STATUS['PERFORMANCE_END'];
        }
        if ($scene == self::SCENE['COMPLETED']) {
            $filter['status'] = self::STATUS['COMPLETED'];
        }
        if ($scene == self::SCENE['CLOSED']) {
            $filter['status'] = self::STATUS['CLOSED'];
        }
        if ($scene == self::SCENE['REFUNDING']) {
            $filter['status'] = self::STATUS['REFUNDING'];
        }
        if ($scene == self::SCENE['REFUND']) {
            $filter['status'] = self::STATUS['REFUND'];
        }
        if (!empty($orderno)) {
            $filter['orderno'] = $orderno;
        }
        if (!empty($oldOrderno)) {
            $filter['oldOrderno'] = $oldOrderno;
        }
        if (!empty($name)) {
            $filter['serviceName'] = $name;
        }
        if (!empty($sellerName)) {
            $filter['sellerEnterpriseName'] = $sellerName;
        }
        if (!empty($buyerCellphone)) {
            $filter['deliveryAddressCellphone'] = $buyerCellphone;
        }
        if (!empty($startTime)) {
            $filter['startTime'] = $startTime;
        }
        if (!empty($endTime)) {
            $filter['endTime'] = $endTime;
        }

        return [$filter, $sort];
    }

    protected function fetchOneAction($id) : bool
    {
        // if (!$this->globalRoleCheck(
        //     IRoleAble::CATEGORY['SERVICE_ORDER'],
        //     IRoleAble::OPERATION['OPERATION_SHOW']
        // )
        // ) {
        //     return false;
        // }

        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $serviceOrder = $this->getRepository()
                    ->scenario(ServiceOrderRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        if ($serviceOrder instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $memberAccountId = $serviceOrder->getBuyerMemberAccount()->getId();

        $buyerEnterprise = $this->fetchBuyerEnterprise($memberAccountId);

        $this->render(new ServiceOrderView($serviceOrder, $buyerEnterprise));
        return true;
    }

    protected function fetchBuyerEnterprise($memberAccountId): Enterprise
    {
        $enterprise = $this->getEnterpriseRepository()
                ->scenario(EnterpriseRepository::FETCH_ONE_MODEL_UN)->fetchOne($memberAccountId);

        return $enterprise;
    }

    //订单导出数据
    public function orderSheetExcel() : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        if (!$this->globalRoleCheck(
            IRoleAble::CATEGORY['SERVICE_ORDER'],
            IRoleAble::OPERATION['OPERATION_DOWNLOAD']
        )
        ) {
            $this->displayError();
            return false;
        }

        list($total, $list) = $this->getOrderList();
        unset($list);

        list($filter, $sort) = $this->filterFormatChange();

        //时间间隔必须在30天内
        $endTime = Core::$container->get('time');
        if (empty($filter['startTime']) && empty($filter['endTime'])) {
            $filter['endTime'] = $endTime;
            $filter['startTime'] = $filter['endTime'] - self::MONTH_LENGTH * 86400;
        }
        if (!empty($filter['startTime']) && empty($filter['endTime'])) {
            $filter['endTime'] = $filter['startTime'] + self::MONTH_LENGTH * 86400;
        }
        if (empty($filter['startTime']) && !empty($filter['endTime'])) {
            $filter['startTime'] = $filter['endTime'] - self::MONTH_LENGTH * 86400;
        }
        
        list($count, $serviceOrderList) = $this->getRepository()
            ->scenario(ServiceOrderRepository::OA_LIST_MODEL_UN)
            ->search($filter, $sort, PAGE, $total);
        
        $this->render(new ServiceOrderListExcelView($serviceOrderList, $count));
        return true;
    }
}
