<?php
namespace ProductMarket\Order\CommonOrder\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Order\CommonOrder\Model\OrderAddress;

use DeliveryAddress\Translator\DeliveryAddressTranslator;

class OrderAddressTranslator implements ITranslator
{
    public function getDeliveryAddressTranslator() : DeliveryAddressTranslator
    {
        return new DeliveryAddressTranslator();
    }

    public function arrayToObject(array $expression, $orderAddress = null)
    {
        return $this->translateToObject($expression, $orderAddress);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderAddress, array $keys = array())
    {
        if (!$orderAddress instanceof OrderAddress) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'cellphone',
                'deliveryAddress'=>[]
            );
        }

        $expression = array();

        if (in_array('cellphone', $keys)) {
            $expression['cellphone'] = $orderAddress->getCellphone();
        }

        if (isset($keys['deliveryAddress'])) {
            $expression['deliveryAddress'] = $this->getDeliveryAddressTranslator()->objectToArray(
                $orderAddress->getDeliveryAddress(),
                $keys['deliveryAddress']
            );
        }

        return $expression;
    }
}
