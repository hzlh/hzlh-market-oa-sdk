<?php
namespace ProductMarket\Order\CommonOrder\Translator;

use Marmot\Interfaces\ITranslator;

use Sdk\ProductMarket\Order\CommonOrder\Model\OrderCommodity;

use ProductMarket\Service\Translator\ServiceTranslator;

class OrderCommodityTranslator implements ITranslator
{
    public function getServiceTranslator() : ServiceTranslator
    {
        return new ServiceTranslator();
    }

    public function arrayToObject(array $expression, $orderCommodity = null)
    {
        return $this->translateToObject($expression, $orderCommodity);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($orderCommodity, array $keys = array())
    {
        if (!$orderCommodity instanceof OrderCommodity) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'number',
                'skuIndex',
                'commodity'=>[]
            );
        }

        $expression = array();

        if (in_array('number', $keys)) {
            $expression['number'] = $orderCommodity->getNumber();
        }

        if (in_array('skuIndex', $keys)) {
            $expression['skuIndex'] = $orderCommodity->getSkuIndex();
        }

        if (isset($keys['commodity'])) {
            $expression['commodity'] = $this->getServiceTranslator()->objectToArray(
                $orderCommodity->getCommodity(),
                $keys['commodity']
            );
        }

        return $expression;
    }
}
